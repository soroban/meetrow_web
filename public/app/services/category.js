angular
    .module('public-app').service('category', function($q) {
  var categories = [];
  var db = firebase.database();
  var categoryAll = db.ref("/category");

  var getFromCategoryId = function (category_id) {
    subCategoryAll.orderByChild("category_id").equalTo(category_id).on("value", function(dataSnapshot) {
      dataSnapshot.forEach(function(cat) {
    	  categories[cat.val().category_id] = cat.val();
      });
    });
    return categories; 
  }
    
  return {
	  getFromCategoryId:getFromCategoryId
  };

});