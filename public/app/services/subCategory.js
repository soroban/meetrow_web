angular
    .module('public-app').service('subCategory', function($q) {
  
  var sub_categories = [];
  var db = firebase.database();
  var category_id = 1;
  var subCategoryAll = db.ref("/sub_category");

  var getAll = function () {
    subCategoryAll.orderByChild("category_id").equalTo(category_id).once("value").then(function(dataSnapshot) {
      dataSnapshot.forEach(function(sub) {
    	  sub_categories[sub.val().sub_category_id] = sub.val();
      });
    });
    
    return sub_categories; 
  }
    
  return {
	  getAll:getAll
  };

});