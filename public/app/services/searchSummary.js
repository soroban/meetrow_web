angular
    .module('public-app').service('searchSummary', function($q) {
  
  var db = firebase.database();
  var category_id = 1;
  var searchAll = db.ref("/search_summary/1");

  var getFromKeyword = function (inputStr, callback) {
    return searchAll.orderByChild("keyword").equalTo(inputStr.toLowerCase()).once("value").then(function (dataSnapshot) {
      callback(dataSnapshot);
    }); 
  }
    
  return {
	  getFromKeyword:getFromKeyword
  };

});