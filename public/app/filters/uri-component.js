;(function() {
  'use strict';

  angular
    .module('public-app')
    .filter('encodeURIComponent', encodeURIComponent)
    .filter('dateFromArea', dateFromArea);

  function encodeURIComponent(){
    return function(input) {
      if(input) {
    	input = input.replace(/\//g, "%2F")
    	input = input.replace(/\s/g, "-");
        return window.encodeURIComponent(input); 
      }
      return "";
    }
  }

  function dateFromArea(){
      return function(date, area) {
          var mNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

          var resultDate = date.match( /(\d{4})-(\d{1,2})-(\d{1,2})(.*)/ );
          var year = resultDate[1];
          var month = resultDate[2];
          var month_num = Number(resultDate[2]);
          var day = resultDate[3];


          if(area == "ja"){
              return  year + "." + month + "." + day;
          }else if(area == "en"){
              return  mNames[month_num] + " " + day + ", " + year;
          }
      }
  }


})();
