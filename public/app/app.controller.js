;(function() {
  'use strict';

  angular
    .module('public-app')
    .controller('AppController', AppController);

  AppController.$inject = ['$rootScope', '$state', '$compile', '$location'];

  function AppController($rootScope, $state, $compile, $location){

    $rootScope.changePage = changePage;
    $rootScope.resetBack = resetBack;
    $rootScope.goMain = goMain;
    $rootScope.myEncode = myEncode;
    $rootScope.myDecode = myDecode;
    $rootScope.replaceUrl = replaceUrl;
    
    function changePage(){
      $state.go($rootScope.chosenPage);
    }
    
    function resetBack(){
        var $body = document.body
        $body.setAttribute('background', '');        	
    }
    
    
    function goMain(){
//    	console.log(location.hash);
//    	var hash = location.hash;
//    	var page = hash.substr(1)
//    	
        $state.go('main');        	
        //$location.path('/map').search({subcategory_id: 2});
    }
    
    function myEncode(input){
    	input = input.replace(/\//g, "%2F");
    	input = input.replace(/\s/g, "-");
        return window.encodeURIComponent(input);
    }

    function myDecode(input){
    	input = window.decodeURIComponent(input);
    	input = input.replace(/-/g, " ");
        return input.replace(/%2F/g, "/");
    }

    function replaceUrl(input){
        input = input.replace(/\//g, "%2F");
        input = input.replace(/\s/g, "-");
        return input;
    }
  }
})();
