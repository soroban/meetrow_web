;(function() {
  'use strict';

  angular
    .module('public-app', [
      'ngRoute',
      'ui.router',
      'ui.bootstrap',
      'uiRouterStyles',
      'ngSanitize',
      'ngCookies',
      'angulartics',
      'angulartics.google.analytics',
      'pascalprecht.translate'
    ])
})();
