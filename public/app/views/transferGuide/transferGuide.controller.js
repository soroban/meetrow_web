;(function() {
    'use strict';

    angular
        .module('public-app').controller('TransferGuideController', TransferGuideController);

    TransferGuideController.$inject = ['$scope', '$sce', 'params', '$uibModalInstance', '$location', '$stateParams', '$state', '$rootScope', '$cookies', '$translate'];

    function TransferGuideController($scope, $sce, params, $uibModalInstance, $location, $stateParams, $state, $rootScope,$cookies, $translate) {
        var db = firebase.database();

        $scope.category = $stateParams.category;
        $scope.sub_category = $stateParams.sub_category;
        $scope.sub_category_id = params.sub_category_id;
        $scope.sub_category_disp = $scope.sub_category.replace(/-/g," ");

        var lang;
        if ($stateParams.lang == "en") {
            $scope.lang = "en";
            $scope.lang_str = "en";
            $scope.main_link = "/" + $scope.lang_str;
            $scope.sub_link = "/" + $scope.lang_str + "/local";
            $translate.use('en');
            $("html").attr("lang", "en");
            $scope.area = "us";
        } else {
            $scope.lang = "ja";
            $scope.lang_str = "";
            $scope.main_link = "/";
            $scope.sub_link = "/global";
            $translate.use('ja');
            $("html").attr("lang", "ja");
            $scope.area = "jp";
        }

        $scope.clickClose = function() {
            $uibModalInstance.close();
        }

        var guides = {};
        console.log($scope.sub_category_id);
        var guide_ref = db.ref("/guide/guide_" + $scope.sub_category_id);

        var count = 0;

        var details_ids = [];
        var details = {};
        var tempGuide = [];
        guide_ref.on("value", function(guideSnapshot) {
            var snapNum = guideSnapshot.numChildren();
            guideSnapshot.forEach(function(guide) {
                var elements = guide.val().elements;
                for(var element_id in elements){
                    var element = elements[element_id]
                    if(element.id){
                        if (details_ids.indexOf(element.id) == -1){
                            details_ids.push(element.id);
                        }
                    }
                }
                var tempArr = {"elments": guide.val().elements, "color": guide.val().color }
                tempGuide.push(tempArr);
            });

            for(var key in details_ids){
                var music_id = details_ids[key];
                var detail_ref = db.ref("/detail_summary/1/" + music_id);
                detail_ref.on("value", function (detail) {
                    details[detail.val().music_id] = detail.val();
                    if(count == details_ids.length-1){
                        $scope.guides = tempGuide;
                        console.log($scope.guides);
                        if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
                            $scope.$apply();
                        }
                    }
                    count++;
                });
            }
        });



        insertMeta();

        function insertMeta() {
//            if($scope.lang == "ja") {
                // if(replaceFlg) {
            //     //     var url ='/map/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category) + "/guide";
            //     //     $location.path(url);
            //     //     $location.replace();
            //     // }
            //     $state.go('guide', {artist: "", index: "", centerId: ""}, {notify: false});
            // }else{
            //     // if(replaceFlg) {
            //     //     var url ='/en/map/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category) + "/guide";
            //     //     $location.path(url);
            //     //     $location.replace();
            //     // }
            //     $state.go('lang_guide', {artist: "", index: "",centerId: ""}, {notify: false});
            // }

            var tempSubCate = $scope.sub_category_disp;
            if($scope.lang != "ja"){
                tempSubCate = $scope.sub_category_disp.replace(/\(.*\)/g, '');
            }

            if($scope.lang == "ja") {
                $state.go('guide', {category: $stateParams.category, sub_category: $stateParams.sub_category}, {notify: false});
            }else{
                $state.go('guide', {category: $stateParams.category, sub_category: $stateParams.sub_category}, {notify: false});
            }

            $translate('transGuide.h1Text').then(function (translations) {
                $scope.h1Text = translations.replace(/\{sub_category_text\}/g, tempSubCate);
            });

            $translate('transGuide.title').then(function (translations) {
                if ($scope.lang == "ja") {
                    var tempTitle = translations.replace(/\{sub_category_text\}/g, tempSubCate);
                    $('link[rel="canonical"]').attr('href', 'https://meetrow.com/guide/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category));
                    $('meta[property="og:url"]').attr('content', 'https://meetrow.com/guide/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category));
                    ga('send', 'pageview', {
                        'page': 'guide/' + $rootScope.replaceUrl($scope.category) + "/" + $rootScope.replaceUrl($scope.sub_category),
                        'title': tempTitle
                    });

                } else {
                    var tempTitle = translations.replace(/\{sub_category_text\}/g, tempSubCate);
                    $('link[rel="canonical"]').attr('href', 'https://meetrow.com/en/guide/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category));
                    $('meta[property="og:url"]').attr('content', 'https://meetrow.com/en/guide/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category));
                    ga('send', 'pageview', {
                        'page': 'en/guide/' + $rootScope.replaceUrl($scope.category) + "/" + $rootScope.replaceUrl($scope.sub_category) + "/" + $scope.artistUrl + "/" + video_ids[index],
                        'title': tempTitle
                    });
                }
                console.log(tempTitle);
                document.title = tempTitle;
                $("title").text(tempTitle);
                $('meta[property="og:title"]').attr('content', tempTitle);
            });

            $("html").find('link[rel="alternate"]').each(function() {
                if($(this).attr("hreflang") == "ja"){
                    $(this).attr("href", 'https://meetrow.com/guide/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category));
                }
                else{
                    $(this).attr("href", "https://meetrow.com/" + $(this).attr("hreflang") + '/guide/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category));
                }
            });
            //
            //
            // $translate('map.h1Text').then(function (translations) {
            //     var tempSubCate = $scope.sub_category_disp;
            //     if($scope.root != "ja"){
            //         tempSubCate = $scope.sub_category_disp.replace(/\(.*\)/g, '');
            //     }
            //     $scope.bread2 = translations.replace(/\{sub_category_text\}/g, tempSubCate);
            // });
            //
            // $translate('detail.h1Text').then(function (translations) {
            //     if ($scope.lang == "ja") {
            //         var tempH1Text = translations.replace(/\{artist\}/g, $scope.artistName).replace(/\{song\}/g, window.decodeURIComponent(video_title[index]));
            //     }else {
            //         var tempH1Text = translations.replace(/\{artist\}/g, $scope.detail.en_name).replace(/\{song\}/g, window.decodeURIComponent(video_title[index]));
            //     }
            //     $scope.h1Text = tempH1Text;
            // });
            //
            // $translate('detail.description').then(function (translations) {
            //     var tempSubCate = $scope.sub_category_disp;
            //     if($scope.lang == "ja"){
            //         var tempDesc = translations.replace(/\{artist\}/g , $scope.artistName).replace(/\{song\}/g , window.decodeURIComponent(video_title[index])).replace(/\{sub_category_text\}/g , tempSubCate);
            //     }else{
            //         tempSubCate = $scope.sub_category_disp.replace(/\(.*\)/g, '');
            //         var tempDesc = translations.replace(/\{artist\}/g , $scope.detail.en_name).replace(/\{song\}/g , window.decodeURIComponent(video_title[index])).replace(/\{sub_category_text\}/g , tempSubCate);
            //     }
            //     $("meta[name=description]").attr('content', tempDesc);
            //     $('meta[property="og:description"]').attr('content', tempDesc);
            // });
            //
            // $translate('common.siteName').then(function (translations) {
            //     $('meta[property="og:site_name"]').attr('content', translations);
            // });
        }

    }
})();