;(function() {
    'use strict';

    angular
        .module('public-app').controller('DetailController', DetailController);

    DetailController.$inject = ['$scope', '$sce', 'params', '$uibModalInstance', '$location', '$stateParams', '$state', '$rootScope', '$cookies', '$translate'];

    function DetailController($scope, $sce, params, $uibModalInstance, $location, $stateParams, $state, $rootScope,$cookies, $translate){
        var lang;
        if($stateParams.lang == "en"){
            $scope.lang = "en";
            $scope.lang_str = "en";
            $scope.main_link = "/" + $scope.lang_str;
            $scope.sub_link = "/" + $scope.lang_str + "/local";
            $translate.use('en');
            $("html").attr("lang", "en");
            $scope.area = "us";
        }else{
            $scope.lang = "ja";
            $scope.lang_str = "";
            $scope.main_link = "/";
            $scope.sub_link = "/global";
            $translate.use('ja');
            $("html").attr("lang", "ja");
            $scope.area = "jp";
        }

        $scope.relationsMap = [];
        $scope.playlist = [];
        $scope.detail = null;
        $scope.title;
        $scope.artistUrl;
        $scope.artistName;
        $scope.searchArtistLink;

        var sub_category_id = params.sub_category_id;
        var music_id = params.music_id;
        var disp_flg = params.disp_flg;

        $scope.category = $stateParams.category;
        $scope.sub_category = $stateParams.sub_category;
        var player;
        var playerReady = false;
        var circle;
        var fontS;
        var processing=false;
        var video_title = [];
        var video_ids = [];
        var video_thumbnail = [];
        var setTime = $translate.instant('map.setTime');
        var stage = $translate.instant('map.stage');

        $scope.sub_category_disp = $scope.sub_category.replace(/-/g," ");

        var tempMap = [];
        var guides = [];
        var relationDetails = {};
        var relCount = 0;
        var arrLength = 0;

        var ua = navigator.userAgent.toLowerCase();
        var windowWidth = window.innerWidth;

        var dayNames = '日月火水木金土'
        var mNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var replaceFlg = false;

        if(window.innerWidth < 1000) {
            if(windowWidth >= 768){
                var yWidth = 599;
                var yHeight = (yWidth * 360)/640;
                var gWidth = 599*2;
                var gHeight = gWidth/2.5;
                var circleR = (gWidth/5.2);
                fontS = 14;
                var circleRLine = 3;
            }else{
                var yWidth = windowWidth;
                var yHeight = (yWidth * 360)/640;
                var gWidth = windowWidth*2;
                var gHeight = gWidth/1.7;
                var circleR = (gWidth/3.6);
                fontS = 14;
                var circleRLine = 3;
            }
        }
        else{
            var yWidth = 560;
            var yHeight = 315;

            var gWidth = 1002 * 2;
            var gHeight = 430;
            fontS = 16;
            var padHeight = window.parent.screen.height - 130 - yHeight;

            if(padHeight > 340){
                gHeight = 550
                fontS = 16;
            }

            var circleR = ((gHeight-5)/2);
            var circleRLine = 6;
        }

        var db = firebase.database();

        $scope.linkMusic = function($event) {
            if(playerReady) {
                var index = $event.currentTarget.getAttribute("data-id");
                var current = index - 1;
                $scope.title = window.decodeURIComponent(video_title[current]);
                if ($scope.lang == "ja") {
                    $state.go('map', {
                        artist: $rootScope.replaceUrl($scope.artistName),
                        index: video_ids[current]
                    }, {notify: false});
                } else {
                    $state.go('lang_map', {
                        artist: $rootScope.replaceUrl($scope.artistName),
                        index: video_ids[current]
                    }, {notify: false});
                }
                processing = true;

                $scope.youtube_id=video_ids[current];
                if($scope.youtube_albums) {
                    if ($scope.youtube_id in $scope.youtube_albums) {
                        $scope.play_albums = $scope.youtube_albums[$scope.youtube_id];
                    } else {
                        $scope.play_albums = null;
                    }
                }

                $translate('detail.albumExp').then(function (translations) {
                    $scope.h2Album = translations.replace(/\{artist\}/g, $scope.artistName);
                });
                insertMeta(parseInt(index), true);
                player.playVideoAt(current);
            }
            $event.preventDefault();
        }

        function drawing() {
            $scope.gWidth = gWidth;
            $scope.gHeight = gHeight;

            var falseRels = [];
            circle = document.getElementById("guide");
            circle.width = gWidth;
            circle.height = gHeight;

            circle.style.width = gWidth/2 + "px";
            circle.style.height = gHeight/2 + "px";

            $(".relations").width(gWidth);
            $(".relations").height(gHeight+50);
            $(".relations").css({'width':$(".relations").width()/2 + 'px', 'height':$(".relations").height()/2 + 'px'});

            var relation_ref = db.ref("/relation/relation_" + sub_category_id);
            relation_ref.orderByChild("from_id").equalTo(parseInt(music_id)).on("value", function(dataSnapshot) {

                var relArr = [];

                dataSnapshot.forEach(function(relation) {
                    relArr[relation.val().to_id] = relation.val();
                });

                var detail_ref = db.ref("/detail_summary/1/");
                var numKey = 0;

                for(var i in relArr){
                    var rel = relArr[i];
                    detail_ref.orderByChild("music_id").equalTo(rel.to_id).on("value", function(detailSnapshot) {
                        detailSnapshot.forEach(function(detail){
                            relationDetails[detail.val().music_id] = detail.val();

                            if(disp_flg[detail.val().music_id]){
                                falseRels.push(relArr[detail.val().music_id]);
                            }
                            else if(detail.val().video_id != undefined) {
                                tempMap = pushRelation(tempMap, relArr[detail.val().music_id]);
                            }else{
                                falseRels.push(relArr[detail.val().music_id]);
                            }
                        });
                        var checkedCol = [];
                        if(Object.keys(relArr).length-1 == numKey){
                            for (var key in falseRels) {
                                var falseRel = falseRels[key];
                                var color = falseRel.color;
                                if(checkedCol.indexOf(color) == -1){
                                    checkedCol.push(color);
                                }
                            }

                            arrLength = checkedCol.length;
                            if (arrLength > 0) {
                                for (var colKey in checkedCol) {
                                    getLineFromColor(checkedCol[colKey], sub_category_id, falseRel);
                                }
                            } else {
                                drawGuide(tempMap);
                            }
                        }
                        numKey++;
                    });
                }
            });
        }

        function getLineFromColor(color, sub_category_id, falseRel){
            var relation_ref = db.ref("/relation/relation_" + sub_category_id);
            var tempDetailArr = {};
            var detail_ref = db.ref("/detail_summary/1/");
            relation_ref.orderByChild("color").equalTo(color).on("value", function(dataSnapshotCol) {
                var preArr = [];
                var postArr = [];
                var detailKey = 0;
                var index = 0
                var keyArr = [];
                var relValueArr = {};

                var snapNum = dataSnapshotCol.numChildren();

                dataSnapshotCol.forEach(function (relationCol) {
                        keyArr.push(relationCol.key);
                        relValueArr[relationCol.key] = relationCol.val();

                        detail_ref.orderByChild("music_id").equalTo(relationCol.val().to_id).on("value", function (detailSnapshot) {
                            detailSnapshot.forEach(function (detail) {
                                tempDetailArr[detail.val().music_id] = detail.val();

                                if(snapNum-1 == detailKey) {
                                    keyArr.sort(
                                        function (a, b) {
                                            return (a < b ? -1 : 1);
                                        }
                                    );

                                    var post = false;
                                    for (var key in keyArr) {
                                        var relId = keyArr[key];
                                        var rel = relValueArr[relId];

                                        if (post == false) {
                                            if (key % 2 == 1) {
                                                preArr.unshift(rel);
                                            }
                                        } else {
                                            if (key % 2 == 0) {
                                                postArr.push(rel);
                                            }
                                        }

                                        if (rel.from_id == music_id) {
                                            post = true
                                        }

                                    }

                                    if (preArr.length > 0) {
                                            for (var key in preArr) {
                                                var pre = preArr[key];
                                                var tempDetail = tempDetailArr[pre.to_id];
                                                if (tempDetail != undefined) {
                                                    if (!disp_flg[tempDetail.music_id]) {
                                                        if (tempDetail.video_id != undefined) {
                                                            relationDetails[tempDetail.music_id] = tempDetail;
                                                            tempMap = pushRelation(tempMap, pre);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                    }

                                    if (postArr.length > 0) {
                                        for (var key in postArr) {
                                            var post = postArr[key];
                                            var tempDetail = tempDetailArr[post.to_id];
                                            if (tempDetail != undefined) {
                                                if (!disp_flg[tempDetail.music_id]) {
                                                    if (tempDetail.video_id != undefined) {
                                                        relationDetails[tempDetail.music_id] = tempDetail;
                                                        tempMap = pushRelation(tempMap, post);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    relCount++;
                                    if (relCount == arrLength
                                    ) {
                                        drawGuide(tempMap);
                                    }

                                }
                            });
                            detailKey++;
                        });
                    index++;
                });
            });
        }

        function pushRelation(tempMap, rel){
            if(rel.to_id in tempMap){
                var tempColor = tempMap[rel.to_id].color;
                tempColor.push(rel.color);
                tempMap[rel.to_id] = {
                    "color":tempColor,
                    "direct":rel.direct,
                    "from_id":rel.from_id,
                    "from_jp_name":rel.from_jp_name,
                    "jp":rel.jp,
                    "to_en_name":rel.to_en_name,
                    "to_id":rel.to_id,
                    "to_jp_name":rel.to_jp_name
                }
            }else{
                tempMap[rel.to_id] = {
                    "color": [rel.color],
                    "direct":rel.direct,
                    "from_id":rel.from_id,
                    "from_jp_name":rel.from_jp_name,
                    "jp":rel.jp,
                    "to_en_name":rel.to_en_name,
                    "to_id":rel.to_id,
                    "to_jp_name":rel.to_jp_name
                }
            }
            return tempMap;
        }

        function addAnchor(text, width, x, y, circle, artist_names, r, id) {
            var artist_name = artist_names[0];
            if (1 in artist_names){
                artist_name += "(" + artist_names[1] + ")";
            }

            var divTag = $("<div></div>", {
                width: width,
                height: width,
                addClass: "rel-div",
                on: {
                    click: function(event) {
                    }
                }
            });

            var station = relationDetails[id];
            var youtube = station.video_id;
            if($scope.lang == "ja") {
                var url = "/map/" + $scope.category + "/" + $scope.sub_category + "/" + artist_name.replace(/\//g, "%2F").replace(/\s/g, "-") + "/" + youtube[0];
            }else{
                var url = "/en/map/" + $scope.category + "/" + $scope.sub_category + "/" + artist_name.replace(/\//g, "%2F").replace(/\s/g, "-") + "/" + youtube[0];
            }


            var spanText = text;
            if ($scope.lang == "ja" && station.jp == false) {
                if(text.toLowerCase() != station.jp_name.toLowerCase()){
                    spanText += "<br>(" + station.jp_name + ")";
                }
            }

            if(station.time != undefined) {
                if (sub_category_id in station.time) {
                    var times = station.time[sub_category_id];
                }
            }

            if(station.day != undefined) {
                if (sub_category_id in station.day) {
                    var days = station.day[sub_category_id];
                }
            }

            var stationText = covertTime(days, times);
            spanText += '<br><span class="pop-time">' + setTime + "</span>" + " " + stationText;

            if(station.stage != undefined) {
                if (station.stage) {
                    if(sub_category_id in station.stage) {
                        var stageText = "";
                        var stages = station.stage[sub_category_id];
                        for (var n = 0; n < stages.length; n++) {
                            stageText += stages[n]
                            if (n != stages.length - 1) {
                                stageText += ", ";
                            }
                        }
                        spanText += '<br><span class="pop-stage">' + stage + "</span>" + " " + stageText;
                    }
                }
            }

            var genre = station.genre;
            if(genre != undefined) {
                spanText += "<br>"
                for (var n = 0; n < genre.length; n++) {
                    spanText += '<span class="pop-genre">' + genre[n] + "</span> ";
                }
            }

            var span = $("<span></span>", {
                html: spanText,
                addClass: "rel-artist-sp",
            });


            var tag = $("<a></a>", {
                text: text,
                width: width,
                href: url,
                addClass: "rel-artist",
                on: {
                    click: function(event) {
                        var now = new Date(),
                            exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
                        $cookies.put('disp_exp','true', {
                            expires : exp
                        });
                        $uibModalInstance.dismiss(id);
                    }
                }
            });

            tag.append(span);
            divTag.css({'position':'absolute'});
            divTag.append(tag);
            var fontSize = fontS/(1.4)-(32.65/width*1.5);
            divTag.offset({ top: y/2+16-(width/2), left: x/2-(width/2)});
            tag.css({"font-size": fontSize + "px", 'font-weight': 'bold'});
            $(".relations").append(divTag);
        }

        function drawGuide(relations) {
            var circle = document.getElementById("guide");
            var ctx3 = circle.getContext('2d');
            ctx3.globalAlpha = 0.6;
            ctx3.fillStyle = "#000000";
            ctx3.lineWidth = 0;
            ctx3.beginPath();
            ctx3.arc(gWidth/2, gHeight/2, circleR,0,Math.PI*2,true);
            ctx3.fill();

            var ctx1 = circle.getContext('2d');
            ctx1.globalAlpha = 1.0;
            ctx1.fillStyle = "#FFFFFF";
            ctx1.lineWidth = 0;
            ctx1.beginPath();
            ctx1.arc(gWidth/2, gHeight/2, circleR * 0.2, 0,Math.PI*2,true);
            ctx1.fill();

            var ctx2 = circle.getContext('2d');
            ctx2.globalAlpha = 0.6;
            ctx2.fillStyle = "#000000";
            ctx2.lineWidth = 0;
            ctx2.beginPath();
            ctx2.arc(gWidth/2, gHeight/2, circleR * 0.18,0,Math.PI*2,true);
            ctx2.fill();

            var locRels = [];
            locRels["90"] = [];
            locRels["135"] = [];
            locRels["180"] = [];
            locRels["m135"] = [];
            locRels["m90"] = [];
            locRels["m45"] = [];
            locRels["0"] = [];
            locRels["45"] = [];

            var c_90 = 0;
            var c_135 = 0;
            var c_180 = 0;
            var c_m135 = 0;
            var c_m90 = 0;
            var c_m45 = 0;
            var c_0 = 0;
            var c_45 = 0;

            relations.forEach(function(relation) {
                if(relation.direct == 90){
                    locRels["90"].push(relation);
                    c_90 += 1;
                }else if(relation.direct == 135){
                    locRels["135"].push(relation);
                    c_135 += 1;
                }else if(relation.direct == 180){
                    locRels["180"].push(relation);
                    c_180 += 1;
                }else if(relation.direct == -135){
                    locRels["m135"].push(relation);
                    c_m135 += 1;
                }else if(relation.direct == -90){
                    locRels["m90"].push(relation);
                    c_m90 += 1;
                }else if(relation.direct == -45){
                    locRels["m45"].push(relation);
                    c_m45 += 1;
                }else if(relation.direct == 0){
                    locRels["0"].push(relation);
                    c_0 += 1;
                }else if(relation.direct == 45){
                    locRels["45"].push(relation);
                    c_45 += 1;
                }
            });

            for(var key in locRels){
                if(key == "90"){
                    var count = c_90;
                }else if(key == "135"){
                    var count = c_135;
                }else if(key == "180"){
                    var count = c_180;
                }else if(key == "m135"){
                    var count = c_m135;
                }else if(key == "m90"){
                    var count = c_m90;
                }else if(key == "m45"){
                    var count = c_m45;
                }else if(key == "0"){
                    var count = c_0;
                }else if(key == "45"){
                    var count = c_45;
                }


                if(count > 0){
                    var locRel = locRels[key];

                    if(count == 1){
                        var locCR = circleR * 0.255;
                    }else{
                        var locCR = (circleR - (circleR * 0.32))/(count)
                    }

                    var locDCR = locCR/count;
                    for(var locKey in locRel){
                        var locr = locRel[locKey];
                        var x = 0;
                        var y = 0;

                        var lside = circleR - (locDCR*1.15);
                        if(locr.direct == 90){
                            var x = gWidth/2 + lside - ((locDCR + 3) * (locKey*2));
                            var y = gHeight/2;
                        }else if(locr.direct == 135){
                            var x = gWidth/2 + (lside/1.414) - (((locDCR + 3) * (locKey*2))/1.414);
                            var y = gHeight/2 - (lside/1.414) + (((locDCR + 3) * (locKey*2))/1.414);
                        }else if(locr.direct == 180){
                            var x = gWidth/2;
                            var y = gHeight/2 - lside + ((locDCR + 3) * (locKey*2));
                        }else if(locr.direct == -135){
                            var x = gWidth/2 - (lside/1.414) + (((locDCR + 3) * (locKey*2))/1.414);
                            var y = gHeight/2 - (lside/1.414) + (((locDCR + 3) * (locKey*2))/1.414);
                        }else if(locr.direct == -90){
                            var x = gWidth/2 - lside + ((locDCR + 3) * (locKey*2));
                            var y = gHeight/2;
                        }else if(locr.direct == -45){
                            var x = gWidth/2 - (lside/1.414) + (((locDCR + 3) * (locKey*2))/1.414);
                            var y = gHeight/2 + (lside/1.414) - (((locDCR + 3) * (locKey*2))/1.414);
                        }else if(locr.direct == 0){
                            var x = gWidth/2;
                            var y = gHeight/2 + lside - ((locDCR + 3) * (locKey*2));
                        }else if(locr.direct == 45){
                            var x = gWidth/2 + (lside/1.414) - (((locDCR + 3) * (locKey*2))/1.414);
                            var y = gHeight/2 + (lside/1.414) - (((locDCR + 3) * (locKey*2))/1.414);
                        }

                        var ctx = circle.getContext('2d');
                        ctx.globalAlpha = 1.0;
                        ctx.fillStyle = "#FFFFFF";
                        ctx.strokeStyle = locr.color[0];
                        ctx.lineWidth = circleRLine;
                        ctx.beginPath();
                        ctx.arc(x, y, locDCR, 0,Math.PI*2,true);
                        ctx.fill();
                        ctx.stroke();

                        if($scope.lang == "ja") {
                            if (locr.jp) {
                                var text = locr.to_jp_name;
                            }
                            else {
                                var text = locr.to_en_name;
                            }
                        }else{
                            var text = locr.to_en_name;
                        }

                        var artists_names = [];
                        if(locr.jp){
                            var en = locr.to_en_name.toLowerCase();
                            var jp = locr.to_jp_name.toLowerCase();
                            if(en != jp){
                                artists_names[0] = locr.to_jp_name;
                                artists_names[1] = locr.to_en_name;
                            }else{
                                artists_names[0] = locr.to_jp_name;
                            }
                        }else{
                            var en = locr.to_en_name.toLowerCase();
                            var jp = locr.to_jp_name.toLowerCase();
                            if(en != jp){
                                artists_names[0] = locr.to_en_name;
                                artists_names[1] = locr.to_jp_name;
                            }else{
                                artists_names[0] = locr.to_en_name;
                            }
                        }

                        var width = (locDCR * 2) * 0.7;
                        var guide = {"x":x, "y":y, "r": locDCR, "text": text}
                        guides[locr.to_id] = guide;
                        addAnchor(text, width/2, x, y, circle, artists_names, locDCR, locr.to_id);
                    }
                }
            }
        }

        $scope.clickClose = function() {
            $uibModalInstance.close(music_id);
        }

        $scope.arraySearch = function(string, array) {
            if (array != undefined){
                if (array.indexOf(string) !== -1) {
                    return true;
                } else {
                    return false;
                }
            }else{
                return false;
            }
        };

        function goHome(){
            $uibModalInstance.close(music_id);
            if ($scope.lang == "ja") {
                $state.go('map', {
                    category: $scope.category,
                    sub_category: $scope.sub_category,
                    artist: '',
                    index: '',
                    centerId: ''
                }, {notify: true});
            } else {
                $state.go('lang_map', {
                    lang: "en",
                    category: $scope.category,
                    sub_category: $scope.sub_category,
                    artist: '',
                    index: '',
                    centerId: ''
                }, {notify: true});
            }

        }

        function detailProc() {
            var en_name = $scope.detail.en_name;
            var jp_name = $scope.detail.jp_name;



            if($scope.detail.jp){
                if(en_name.toLowerCase() != jp_name.toLowerCase()){
                    $scope.artistName = jp_name + "(" + en_name + ")";
                }else{
                    $scope.artistName = jp_name;
                }
            }else{
                if(en_name.toLowerCase() != jp_name.toLowerCase()){
                    $scope.artistName = en_name + "(" + jp_name + ")";
                }else{
                    $scope.artistName = en_name;
                }
            }


            $translate('detail.otherAlbumLink').then(function (translations) {
                $scope.otherAlbumLink = translations.replace(/\{artist\}/g, $scope.artistName);
            });

            $translate('detail.popularAlbum').then(function (translations) {
                $scope.popularAlbum = translations.replace(/\{artist\}/g, $scope.artistName);
            });


            $scope.exp = true;

            if ($scope.lang == "ja") {
                $translate('detail.youtube').then(function (translations) {
                    $scope.youtube = translations.replace(/\{artist\}/g, $scope.artistName);
                });
                if($scope.detail.popular_album) {
                    if ($scope.detail.popular_album.jp) {
                        $scope.popular_albums = $scope.detail.popular_album.jp;
                        $scope.artistLink = $scope.detail.popular_album.jp[0].artistLink;
                    }
                }
                if($scope.detail.youtube_album) {
                    if ($scope.detail.youtube_album.jp) {
                        $scope.youtube_albums = $scope.detail.youtube_album.jp;
                        $scope.artistLink = $scope.detail.popular_album.jp[0].artistLink;
                    }
                }
            }else{
                $translate('detail.youtube').then(function (translations) {
                    $scope.youtube = translations.replace(/\{artist\}/g, $scope.detail.en_name);
                });
                if($scope.detail.popular_album) {
                    if ($scope.detail.popular_album.us) {
                        $scope.popular_albums = $scope.detail.popular_album.us;
                        $scope.artistLink = $scope.detail.popular_album.us[0].artistLink;
                    }
                }
                if($scope.detail.youtube_album) {
                    if ($scope.detail.youtube_album.us) {
                        $scope.youtube_albums = $scope.detail.youtube_album.us;
                        $scope.artistLink = $scope.detail.popular_album.us[0].artistLink;
                    }
                }
            }
            $scope.artistUrl = $rootScope.myEncode($scope.artistName);

            if(window.innerWidth < 1000) {
                var spanclass =  "detail-genre-mob";
            }else{
                var spanclass =  "detail-genre";
            }

            $translate('detail.musicGenre').then(function (translations) {
                if($scope.detail.genre) {
                    var genre_arr = $scope.detail.genre;
                    var genre_temp_span = "";
                    for (var key in genre_arr) {
                        if(key == 0){
                            genre_temp_span += translations + " ";
                        }
                        genre_temp_span += "<span class='" + spanclass + "'>" + genre_arr[key] + "</span> ";
                    }

                    $scope.genre_span = genre_temp_span;
                }
            });

            video_ids = $scope.detail.video_id;
            video_thumbnail = $scope.detail.video_thumbnail;
            video_title = $scope.detail.video_title;

            var playList = "";
            var splayId = "";
            for(var key in video_ids){
                if(key == 0){
                    splayId = video_ids[key];
                } else{
                    playList += video_ids[key] + ",";
                }
            }

            for(var key in video_title){
                var index = parseInt(key)+1;
                var tempList = {"index":index,"youtube_id":video_ids[key], "title": decodeURIComponent(video_title[key]), "thumbnail": decodeURIComponent(video_thumbnail[key])}
                $scope.playlist.push(tempList);
            }

            var index = 0;
            var check = false;
            if($stateParams.artist != null && $stateParams.index != null){
                if($stateParams.index <= video_title.length && $stateParams.index > 0){
                    index = parseInt($stateParams.index)-1;
                    replaceFlg = true;
                    check = true;
                }else{
                    var check = false;
                    for(var key in video_ids){
                        if($stateParams.index == video_ids[key]){
                            index = parseInt(key);
                            check = true;
                        }
                    }
                }
                if(check == false){
                    goHome();
                }
            }

            $scope.youtube_id=video_ids[index];
            if($scope.youtube_albums) {
                if ($scope.youtube_id in $scope.youtube_albums) {
                    $scope.play_albums = $scope.youtube_albums[$scope.youtube_id];
                } else {
                    $scope.play_albums = null;
                }
            }

            $translate('detail.albumExp').then(function (translations) {
                $scope.h2Album = translations.replace(/\{artist\}/g, $scope.artistName);
            });

            $scope.title = window.decodeURIComponent(video_title[index]);
            if($scope.detail.stage != undefined) {
                if (sub_category_id in $scope.detail.stage) {
                    $scope.stages = $scope.detail.stage[sub_category_id];
                }
            }

            if($scope.detail.time != undefined) {
                if (sub_category_id in $scope.detail.time) {
                    $scope.times = $scope.detail.time[sub_category_id];
                }
            }

            if($scope.detail.day != undefined) {
                if (sub_category_id in $scope.detail.day) {
                    $scope.days = $scope.detail.day[sub_category_id];
                }
            }

            $scope.timesText = covertTime($scope.days, $scope.times);

            if($scope.lang == "ja") {
                if(replaceFlg) {
                    var url ='/map/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category) + "/" + $scope.artistUrl + "/" + video_ids[index];
                    $location.path(url);
                    $location.replace();
                }
                $state.go('map', {artist: $rootScope.replaceUrl($scope.artistName), index: video_ids[index], centerId: ""}, {notify: false});
            }else{
                if(replaceFlg) {
                    var url ='/en/map/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category) + "/" + $scope.artistUrl + "/" + video_ids[index];
                    $location.path(url);
                    $location.replace();
                }
                $state.go('lang_map', {artist: $rootScope.replaceUrl($scope.artistName), index: video_ids[index],centerId: ""}, {notify: false});
            }

            processing = true;
            insertMeta(index+1, true);
            if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
                $scope.$apply();
            }

            youtube(yWidth, yHeight, video_ids[0], playList, index, video_title);

            if(!$cookies.get('disp_exp')){
                $("#exp").show();
            }
        }

        $scope.callYoutube = function() {
            //if(isiPhone || isAndroid || isiPad || isAndroidTablet) {
            if(window.innerWidth < 1000) {
                $("#player-mob").width(yWidth);
                $("#player-mob").height(yHeight);
            }

            if(music_id) {
                var detail_ref = db.ref("/detail_summary/1/" + music_id);
                detail_ref.on("value", function (detail) {
                    if (detail.numChildren() == 0) {
                        goHome();
                    }

                    $scope.detail = detail.val();
                    drawing();
                    detailProc();
                });
            }else{
                var detail_ref = db.ref("/detail_summary/1/");
                detail_ref.orderByChild("url_name").equalTo($stateParams.artist).on("value", function(detailSnapshot) {
                    if (detailSnapshot.numChildren() == 0) {
                        goHome();
                    }
                    detailSnapshot.forEach(function(detail) {
                        $scope.detail = detail.val();
                        music_id = detail.val().music_id;
                        drawing();
                        detailProc();
                    });
                });

            }
        }

        function youtube(yWidth, yHeight, video_id, playList, indexCurrent, video_title, click){
            if(document.getElementById('iframe_api') === null){
                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/iframe_api";
                tag.id = "iframe_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            }
            else
                runPlayer(click);

            function runPlayer(click){
                if(click){
                    return onClick();
                }
                //if(isiPhone || isAndroid || isiPad || isAndroidTablet) {
                if(window.innerWidth < 1000) {
                    var playerName = "player-mob";
                }else{
                    var playerName = "player";
                }

                var index = 999999;
                player = new YT.Player(playerName, {
                    playerVars: {
                        playsinline: 1,
                        autoplay: 1,
                        showinfo: 0,
                        playlist: playList,
                    },
                    height: yHeight,
                    width: yWidth,
                    videoId: video_id,
                    events: {
                        onStateChange: onPlayerStateChange,
                        onReady: onPlayerReady
                    }
                });

                function onPlayerStateChange(e) {
                    //if(isiPhone || isAndroid || isiPad || isAndroidTablet) {
                    if(window.innerWidth < 1000) {
                        var ListName = ".music-scrolllist-mob";
                    }else{
                        var ListName = ".music-scrolllist";
                    }

                    if (e.data == YT.PlayerState.ENDED) {
                        $scope.$emit('VideoEnded');
                    }

                    var nowIndex = e.target.getPlaylistIndex();
                    var ol = $(ListName);

                    if(index == 999999){
                        var li = $(ListName + ' li').eq(nowIndex);
                        li.css('background-color', '#3a3a3a');

                        var indexSpan = li.children('span').eq(0);
                        indexSpan.text("▶");
                        indexSpan.css("color", "#c03636");

                        if(li.position() != null && ol.position() != null){
                            $(ListName).scrollTop(li.position().top-ol.position().top);
                        }
                    }
                    if(index != nowIndex) {
                        var li = $(ListName + ' li').eq(nowIndex);
                        li.css('background-color', '#3a3a3a');
                        var indexSpan = li.children('span').eq(0);
                        indexSpan.text("▶");
                        indexSpan.css("color", "#c03636");

                        var nowY = $(ListName).scrollTop()
                        $(ListName).scrollTop(li.position().top + nowY - ol.position().top);

                        var dispIndex = index + 1;
                        var oldLi = $(ListName + ' li').eq(index);
                        oldLi.css('background-color', '');
                        var oldSpan = oldLi.children('span').eq(0);
                        oldSpan.text(dispIndex);
                        oldSpan.css("color", "#b8b8b8");

                        index = nowIndex;
                        if (index < 1) {
                            index = 0;
                        }

                        if (processing == false) {
                            $scope.title = window.decodeURIComponent(video_title[index]);

                            var indexNo = parseInt(index+1)

                            $translate('detail.albumExp').then(function (translations) {
                                $scope.h2Album = translations.replace(/\{artist\}/g, $scope.artistName);
                            });

                            if($scope.lang == "ja") {
                                $state.go('map', {'artist': $rootScope.replaceUrl($scope.artistName), 'index': video_ids[index]}, {notify: false});
                            }else{
                                $state.go('lang_map', {'artist': $rootScope.replaceUrl($scope.artistName), 'index': video_ids[index]}, {notify: false});
                            }
                            $scope.youtube_id=video_ids[index];

                            if($scope.youtube_albums) {
                                if ($scope.youtube_id in $scope.youtube_albums) {
                                    $scope.play_albums = $scope.youtube_albums[$scope.youtube_id];
                                } else {
                                    $scope.play_albums = null;
                                }
                            }
                            insertMeta(indexNo, true);
                        }
                        processing = false;
                    }
                }
                function onPlayerReady(e) {
                    playerReady = true;
                    e.target.playVideoAt(indexCurrent);
                }

                function onClick() {
                    player.playVideoAt(indexCurrent);
                }
            }
            window.onYouTubeIframeAPIReady = function () {
                runPlayer();
            }
        }

        function insertMeta(indexNo, count) {
            var index = indexNo - 1;
            $translate('detail.h2Text').then(function (translations) {
                var tempSubCate = $scope.sub_category_disp;
                if($scope.root != "ja"){
                    tempSubCate = $scope.sub_category_disp.replace(/\(.*\)/g, '');
                }
                if ($scope.lang == "ja") {
                    $scope.h2Text = translations.replace(/\{artist\}/g, $scope.artistName).replace(/\{sub_category_text\}/g, tempSubCate);
                }else{
                    $scope.h2Text = translations.replace(/\{artist\}/g, $scope.detail.en_name).replace(/\{sub_category_text\}/g, tempSubCate);
                }
            });

            $translate('detail.title').then(function (translations) {
                if ($scope.lang == "ja") {
                    var tempTitle = translations.replace(/\{artist\}/g , $scope.artistName).replace(/\{song\}/g , window.decodeURIComponent(video_title[index]));
                    $('link[rel="canonical"]').attr('href', 'https://meetrow.com/map/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category) + "/" + $scope.artistUrl + "/" + video_ids[index]);
                    $('meta[property="og:url"]').attr('content', 'https://meetrow.com/map/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category) + "/" + $scope.artistUrl + "/" + video_ids[index]);
                    if(count) {
                        ga('send', 'pageview', {
                            'page': 'map/' + $rootScope.replaceUrl($scope.category) + "/" + $rootScope.replaceUrl($scope.sub_category) + "/" + $scope.artistUrl + "/" + video_ids[index],
                            'title': tempTitle
                        });
                    }

                } else {
                    var tempTitle = translations.replace(/\{artist\}/g , $scope.detail.en_name).replace(/\{song\}/g , window.decodeURIComponent(video_title[index]));

                    $('link[rel="canonical"]').attr('href', 'https://meetrow.com/en/map/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category) + "/" + $scope.artistUrl + "/" + video_ids[index]);
                    $('meta[property="og:url"]').attr('content', 'https://meetrow.com/en/map/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category) + "/" + $scope.artistUrl + "/" + video_ids[index]);
                    if(count) {
                        ga('send', 'pageview', {
                            'page': 'en/map/' + $rootScope.replaceUrl($scope.category) + "/" + $rootScope.replaceUrl($scope.sub_category) + "/" + $scope.artistUrl + "/" + video_ids[index],
                            'title': tempTitle
                        });
                    }
                }
                document.title = tempTitle;
                $("title").text(tempTitle);
                $('meta[property="og:title"]').attr('content', tempTitle);
            });

            $("html").find('link[rel="alternate"]').each(function() {
                if($(this).attr("hreflang") == "ja"){
                    $(this).attr("href", 'https://meetrow.com/map/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category) + "/" + $scope.artistUrl + "/" + video_ids[index]);
                }
                else{
                    $(this).attr("href", "https://meetrow.com/" + $(this).attr("hreflang") + '/map/' + $rootScope.myEncode($scope.category) + "/" + $rootScope.myEncode($scope.sub_category) + "/" + $scope.artistUrl + "/" + video_ids[index]);
                }
            });


            $translate('map.h1Text').then(function (translations) {
                var tempSubCate = $scope.sub_category_disp;
                if($scope.root != "ja"){
                    tempSubCate = $scope.sub_category_disp.replace(/\(.*\)/g, '');
                }
                $scope.bread2 = translations.replace(/\{sub_category_text\}/g, tempSubCate);
            });

            $translate('detail.h1Text').then(function (translations) {
                if ($scope.lang == "ja") {
                    var tempH1Text = translations.replace(/\{artist\}/g, $scope.artistName).replace(/\{song\}/g, window.decodeURIComponent(video_title[index]));
                }else {
                    var tempH1Text = translations.replace(/\{artist\}/g, $scope.detail.en_name).replace(/\{song\}/g, window.decodeURIComponent(video_title[index]));
                }
                $scope.h1Text = tempH1Text;
            });

            $translate('detail.description').then(function (translations) {
                var tempSubCate = $scope.sub_category_disp;
                if($scope.lang == "ja"){
                    var tempDesc = translations.replace(/\{artist\}/g , $scope.artistName).replace(/\{song\}/g , window.decodeURIComponent(video_title[index])).replace(/\{sub_category_text\}/g , tempSubCate);
                }else{
                    tempSubCate = $scope.sub_category_disp.replace(/\(.*\)/g, '');
                    var tempDesc = translations.replace(/\{artist\}/g , $scope.detail.en_name).replace(/\{song\}/g , window.decodeURIComponent(video_title[index])).replace(/\{sub_category_text\}/g , tempSubCate);
                }
                $("meta[name=description]").attr('content', tempDesc);
                $('meta[property="og:description"]').attr('content', tempDesc);
            });

            $translate('common.siteName').then(function (translations) {
                $('meta[property="og:site_name"]').attr('content', translations);
            });
        }
        function covertTime(days, times){
            var dayBase = [];
            for (var key in days) {
                var dayAndArea = days[key].split("-");
                var area = null;

                if(dayAndArea.length > 1){
                    var dayArr = dayAndArea[1].split("/");
                    if(0 in dayAndArea){
                        area = dayAndArea[0];
                    }
                }else{
                    var dayArr = dayAndArea[0].split("/");
                }

                var time = null;
                if(times != undefined) {
                    if (key in times) {
                        time = times[key];
                    }
                }

                var dateObj = new Date(dayArr[0], dayArr[1] - 1, dayArr[2]);
                var day = dateObj.getDay();
                var dayName = dayNames[day];

                dayBase.push({
                    year: dayArr[0],
                    month: dayArr[1],
                    day: dayArr[2],
                    dayName: dayName,
                    area: area,
                    time: time
                });
            }

            var dayText ="";
            if (dayBase.length > 1) {
                for (var key in dayBase) {
                    var date = dayBase[key];
                    if (key == 0) {
                        if ($scope.lang == "ja") {
                            dayText = date.year + "." + date.month + "." + date.day + "[" + date.dayName + "]";
                            if (date.area != null) {
                                dayText += "-" + date.area;
                            }
                            if (date.time != null) {
                                dayText += " " + date.time;
                            }
                        } else {
                            dayText = mNames[date.month - 1] + " " + date.day;

                            if (date.time != null) {
                                dayText += " at " + date.time;
                            }
                            if (date.area != null) {
                                dayText += " (" + date.area + ")";
                            }
                        }


                    } else if (key == dayBase.length - 1) {
                        if ($scope.lang == "ja") {
                            dayText += "/" + date.month + "." + date.day + "[" + date.dayName + "]";
                            if (date.area != null) {
                                dayText += "-" + date.area;
                            }
                            if (date.time != null) {
                                dayText += " " + date.time;
                            }
                        }else{
                            dayText += ", " + date.day;
                            if (date.time != null) {
                                dayText += " at " + date.time;
                            }
                            if (date.area != null) {
                                dayText += " (" + date.area + ")";
                            }

                            dayText += ",  " + date.year;
                        }
                    }
                    else {
                        if ($scope.lang == "ja") {
                            dayText += "/" + date.month + "." + date.day + "[" + date.dayName + "]";
                            if (date.area != null) {
                                dayText += "-" + date.area;
                            }
                            if (date.time != null) {
                                dayText += " " + date.time;
                            }
                        }else{
                            dayText += ", " + date.day;
                            if (date.time != null) {
                                dayText += " at " + date.time;
                            }
                            if (date.area != null) {
                                dayText += " (" + date.area + ")";
                            }
                        }
                    }
                }
            }else if (dayBase.length == 1) {
                var date = dayBase[0];
                if ($scope.lang == "ja") {
                    if (date.area != null) {
                        dayText += date.area + " - ";
                    }
                    dayText += date.year + "." + date.month + "." + date.day + "[" + date.dayName + "]";
                    if (date.area != null) {
                        dayText += "-" + date.area;
                    }
                    if (date.time != null) {
                        dayText += " " + date.time;
                    }

                } else {
                    dayText = mNames[date.month - 1] + " " + date.day;
                    if (date.time != null) {
                        dayText += " at " + date.time;
                    }
                    if (date.area != null) {
                        dayText += " (" + date.area + ")";
                    }
                    dayText += ",  " + date.year;
                }
            }
            return dayText;
        }
    }
})();