;(function() {
    'use strict';

    angular
      .module('public-app')
      .controller('MapController', MapController);

    MapController.$inject = ['$scope', '$stateParams', '$timeout', '$element', '$uibModal', '$location', '$state', '$rootScope', '$translate'];

    function MapController($scope, $stateParams, $timeout, $element, $uibModal, $location, $state, $rootScope, $translate){
	    var h = $(window).height();
    	$('#container').css('display','none');
	    $('#fountanWrap').height(h).css('display','block');

        var lang;
        if($stateParams.lang == "en"){
            $scope.lang = "en";
            $scope.lang_str = "en";
            $scope.main_link = "/" + $scope.lang_str;
            $scope.main_link = "/" + $scope.lang_str;
            $scope.sub_link = $scope.lang_str + "/local";
            $translate.use('en');
            $scope.main = "globe_nosel";
            $scope.sub = "japan_nosel";
            $("html").attr("lang", "en");
            var setTime = "Set Time";
            var stage = "Stage";
        }else{
            $scope.lang = "ja";
            $scope.lang_str = "";
            $scope.main_link = "/";
            $scope.sub_link = "/global";
            $translate.use('ja');
            $scope.main = "japan_nosel";
            $scope.sub = "globe_nosel";
            $("html").attr("lang", "ja");
            var setTime = "タイムテーブル";
            var stage = "ステージ";
        }

        function stopload(x, y, id){
  		  	$('#fountanWrap').delay(600).fadeOut(1000).queue(function() {
  		  		$('#container').css('display','');
  			    setCenter(x, y);
            	    if(id != null){
            	  	    if(id in disp_flg){
	  	    		    }else{
              	            flashText(id);
            	  	    }
			        }
			});
        }

		$scope.suggestion = [];
		$scope.name_sub;
		$scope.h1Text;

		$scope.lowerRight;
		$scope.tickets;
		$scope.ticketCheck=false;
		$scope.byday = 99999;
		$scope.detail = false;

        var press=false;
        var dayNames = '日月火水木金土'
        var mNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		var category_name = window.decodeURIComponent($stateParams.category).replace(/-/g," ");
		var artist = $stateParams.artist;
		var paramCenterId = $location.search().centerId;
		var didFirstTap = false;
        var didFirstX = null;
        var didFirstY = null;
        var mapStations = [];
		var mapLines = [];
		var textAreas = [];
		var artists = [];
		var disp_flg = [];
		var centerId = null;
        var checkOver = false;
        var checkOverNo;
		var category_id = null;
		var sub_category_id = null;
		var sub_category_tables = [];
		var modalInstance;

		var db = firebase.database();
		var categoryAll = db.ref("/category");
		var subCategoryAll = db.ref("/sub_category");
		var searchAll = db.ref("/search_summary/1");
		var zoomInDisable = false;
		var zoomOutDisable = false;
		var canvas;
		var ctx;
		var openFlg = false;
        var directOpen = false;

		var radius = 25;
		var circleLineWidth = 7;
		var lineWidth = 10;
		var arc = 30;
		var m = 88;
		var l = 54;

		var nowWidth;
		var nowHeight;

		var defaultScale = 0.65;
		var minScale = 0.55;
		var maxScale = 0.75;
		var sumScale = 0.1;
		var scale = defaultScale;
		var alpha = 0;

		var reduction = 0.5;
		var windowWidth = window.innerWidth;
		var windowHeight = window.innerHeight;

		var diffW = ((windowWidth + 100) / scale);
		var diffH = ((windowHeight + 100) / scale);

		var width = ((100 * m) * reduction) + (diffW * 2);
		var height = ((100 * l) * reduction) + (diffH * 2);

		var myFont = "Bold 56px 'Helvetica'"

        var ratio= scale/defaultScale;
        var fontSize = 11 * ratio;
        var den = scale/2;
        var textWidth = 40*ratio;
        var sub_category_day;

        nowWidth = width * scale;
		nowHeight = height * scale;
		$('#map').attr('width', nowWidth);
		$('#map').attr('height', nowHeight);

		$("#map-al").attr('width', nowWidth);
		$("#map-al").attr('height', nowHeight);

		var device = getDevice();
		if(device != "other"){
			$("#inner").css("overflow","auto");
		}

		if( scale - sumScale < minScale){
		  	$("#sub-wrapper").fadeTo("fast", 0.3);
		  	zoomOutDisable = true;
		  	$("#sub-wrapper").css("pointer-events","none");
		}else{
		  	$("#sub-wrapper").fadeTo("fast", 0.9);
		  	$("#sub-wrapper").css("pointer-events","");
		  	zoomOutDisable = false;
		}

		$scope.daysParam = $stateParams.days

        $scope.selectList = function(value) {
            var priJson = [];
            var retJson = [];
            var value = value.replace(/[ぁ-ん]/g, function(s) {
                return String.fromCharCode(s.charCodeAt(0) + 0x60);
            });

            return searchAll.orderByChild("keyword").equalTo(value.toLowerCase()).once("value").then(function (dataSnapshot) {
                dataSnapshot.forEach(function(search) {
                    if(parseInt(search.val().sub_category_id) == parseInt(sub_category_id)){
                        if($scope.lang == "en"){
							var disp = "displaying";
                        }else {
                            var disp = "表示中";
                        }
                        priJson.push({"disp":search.val().disp_str, "fes":disp,"id":search.val().id, "transition": false});
                    }else{
                        var subCat = sub_category_tables[search.val().sub_category_id];
                        if($scope.lang == "en"){
                            var fes = subCat.name+"("+ subCat.name_sub +")";
                            retJson.push({"disp":search.val().disp_str, "fes":fes,"url_param":subCat.name+"("+ subCat.name_sub +")", "id":search.val().id, "transition": true});
                        }else {
                            var fes = subCat.name;
                            retJson.push({"disp":search.val().disp_str, "fes":fes,"url_param":subCat.name+"("+ subCat.name_sub +")", "id":search.val().id, "transition": true});
                        }
                    }
                });
                return priJson.concat(retJson);
            });
        }

		categoryAll.orderByChild("name").equalTo(category_name).on("value", function(dataSnapshotPa) {
			if(dataSnapshotPa.numChildren() == 0){
                goMain();
			}

	  		dataSnapshotPa.forEach(function(category) {
				category_id = category.val().category_id;
                var urlParam = $stateParams.sub_category.match(/(.+)\((.+)\)/);
                if(1 in urlParam){
                    var name = urlParam[1].replace(/%2F/g, "/").replace(/-/g, " ");
                }else{
                    goMain();
                }

                if(2 in urlParam){
                    var name_sub = urlParam[2].replace(/%2F/g, "/").replace(/-/g, " ");
                }else{
                    goMain();
                }

                // console.log(name);
                // console.log(name_sub);

                subCategoryAll.orderByChild("name").equalTo(name).on("value", function(dataSnapshot) {
 //               subCategoryAll.on("value", function(dataSnapshot) {
					var check = false;
					dataSnapshot.forEach(function(sub_category) {
					if(sub_category.val().name != undefined) {
//						var tempSub = sub_category.val().name.replace(/\//g, "%2F").replace(/\s/g, "-") + "(" + sub_category.val().name_sub.replace(/\//g, "%2F").replace(/\s/g, "-") + ")";
						if (sub_category.val().name_sub == name_sub) {
							if (sub_category.val().state == 2) {
                                sub_category_id = sub_category.val().sub_category_id;
                                $scope.name = sub_category.val().name;
                                $scope.name_sub = sub_category.val().name_sub;
                                $scope.out_link = sub_category.val().out_link;
                                sub_category_day = sub_category.val().day;

                                canvas = $element.find('canvas');
                                canvas.css({'width': nowWidth / 2 + 'px', 'height': nowHeight / 2 + 'px'});

                                $("#map-al").css({'width': nowWidth / 2 + 'px', 'height': nowHeight / 2 + 'px'});

                                ctx = canvas[0].getContext('2d');
                                ctx.font = myFont;
                                ctx.scale(scale, scale);

                                centerId = sub_category.val().center_id;

                                if ($stateParams.artist != null && $stateParams.index != null) {
                                    show();
                                    directOpen = true;
                                }

                                if ($stateParams.guide == true) {
                                    showGuide();
                                    directOpen = true;
                                }

                                if ($stateParams.lang == "en") {
                                    if (sub_category.val().country == "jp") {
                                        $scope.main = "globe_nosel";
                                        $scope.sub = "japan";
                                    } else {
                                        $scope.main = "globe";
                                        $scope.sub = "japan_nosel";
                                    }

                                    $translate('map.h1Text').then(function (translations) {
                                        var sub_category_text = sub_category.val().name;
                                        $scope.h1Text = translations.replace(/\{sub_category_text\}/g, sub_category_text);
                                    });
                                } else {
                                    if (sub_category.val().country == "jp") {
                                        $scope.main = "japan";
                                        $scope.sub = "globe_nosel";
                                    } else {
                                        $scope.main = "japan_nosel";
                                        $scope.sub = "globe";
                                    }

                                    $translate('map.h1Text').then(function (translations) {
                                        var sub_category_text = sub_category.val().name + "(" + sub_category.val().name_sub + ")"
                                        $scope.h1Text = translations.replace(/\{sub_category_text\}/g, sub_category_text);
                                    });
                                }


                                if ($scope.lang == "en") {
                                    if (sub_category.val().ticket_en) {
                                        $scope.tickets = [];
                                        var tickets = sub_category.val().ticket_en;
                                        for (var key in tickets) {
                                            var tempTicket = tickets[key];
                                            if (tempTicket.state == 1) {
                                                $scope.ticketCheck = true;
                                                $scope.tickets.push(tempTicket);
                                            }
                                        }
                                    }
                                    if (sub_category.val().lrAdd_en) {
                                        if (sub_category.val().lrAdd_en.state == 1) {
                                            $scope.lowerRight = true;
                                            $scope.addText = sub_category.val().lrAdd.text;
                                            $scope.atag = sub_category.val().lrAdd.tag;
                                        }
                                    }

                                    var sub_category_text = $scope.name;
                                    $('link[rel="canonical"]').attr('href', 'https://meetrow.com/' + $scope.lang + '/map/' + $rootScope.myEncode(category_name) + "/" + $rootScope.myEncode($scope.name + "(" + $scope.name_sub + ")"));
                                    $('meta[property="og:url"]').attr('content', 'https://meetrow.com/' + $scope.lang + '/map/' + $rootScope.myEncode(category_name) + "/" + $rootScope.myEncode($scope.name + "(" + $scope.name_sub + ")"));
                                } else {
                                    if (sub_category.val().ticket) {
                                        $scope.tickets = [];
                                        var tickets = sub_category.val().ticket;
                                        for (var key in tickets) {
                                            var tempTicket = tickets[key];
                                            if (tempTicket.state == 1) {
                                                $scope.ticketCheck = true;
                                                $scope.tickets.push(tempTicket);
                                            }
                                        }
                                    }
                                    if (sub_category.val().lrAdd) {
                                        if (sub_category.val().lrAdd.state == 1) {
                                            $scope.lowerRight = true;
                                            $scope.addText = sub_category.val().lrAdd.text;
                                            $scope.atag = sub_category.val().lrAdd.tag;
                                        }
                                    }

                                    var sub_category_text = $scope.name + "(" + $scope.name_sub + ")";
                                    $('link[rel="canonical"]').attr('href', 'https://meetrow.com/map/' + $rootScope.myEncode(category_name) + "/" + $rootScope.myEncode($scope.name + "(" + $scope.name_sub + ")"));
                                    $('meta[property="og:url"]').attr('content', 'https://meetrow.com/map/' + $rootScope.myEncode(category_name) + "/" + $rootScope.myEncode($scope.name + "(" + $scope.name_sub + ")"));
                                }

                                $("html").find('link[rel="alternate"]').each(function () {
                                    if ($(this).attr("hreflang") == "ja") {
                                        $scope.urlJa = /map/ + $rootScope.myEncode(category_name) + "/" + $rootScope.myEncode($scope.name + "(" + $scope.name_sub + ")");
                                        $(this).attr("href", 'https://meetrow.com' + $scope.urlJa);
                                    }
                                    else {
                                        $scope.urlEn = $(this).attr("hreflang") + '/map/' + $rootScope.myEncode(category_name) + "/" + $rootScope.myEncode($scope.name + "(" + $scope.name_sub + ")");
                                        $(this).attr("href", 'https://meetrow.com' + $scope.urlEn);
                                    }
                                });

                                $translate('common.keywords').then(function (translations) {
                                    $("meta[name=keywords]").attr('content', translations);
                                });
                                $translate('map.title').then(function (translations) {
                                    document.title = sub_category_text + translations;
                                    $("title").text(sub_category_text + translations);
                                    $('meta[property="og:title"]').attr('content', sub_category_text + translations);
                                });

                                $translate('map.description').then(function (translations) {
                                    $("meta[name=description]").attr('content', translations.replace(/\{sub_category_text\}/g, sub_category_text));
                                    $('meta[property="og:description"]').attr('content', translations.replace(/\{sub_category_text\}/g, sub_category_text));
                                });

                                $translate('common.siteName').then(function (translations) {
                                    $('meta[property="og:site_name"]').attr('content', translations);
                                });

                                var days = sub_category.val().day;
                                var dayBase = [];
                                for (var key in days) {
                                    var dayAndArea = days[key].split("-");
                                    if (dayAndArea.length > 1) {
                                        var dayArr = dayAndArea[1].split("/");
                                        var dateObj = new Date(dayArr[0], dayArr[1] - 1, dayArr[2]);
                                        var day = dateObj.getDay();
                                        var dayName = dayNames[day];

                                        dayBase.push({
                                            year: dayArr[0],
                                            month: dayArr[1],
                                            day: dayArr[2],
                                            dayName: dayName,
                                            area: dayAndArea[0]
                                        });
                                    } else {
                                        var dayArr = days[key].split("/");
                                        var dateObj = new Date(dayArr[0], dayArr[1] - 1, dayArr[2]);
                                        var day = dateObj.getDay();
                                        var dayName = dayNames[day];

                                        dayBase.push({
                                            year: dayArr[0],
                                            month: dayArr[1],
                                            day: dayArr[2],
                                            dayName: dayName,
                                            area: null
                                        });
                                    }
                                }

                                if (dayBase.length > 1) {
                                    $scope.dates = new Array();
                                    var now = new Date();
                                    var todayAtMidn = new Date(now.getFullYear(), now.getMonth() + 1, now.getDate());

                                    var firstDate = dayBase[0];
                                    var finalDate = dayBase[dayBase.length - 1];
                                    var startDate = new Date(firstDate.year, firstDate.month, firstDate.day);
                                    var lastDate = new Date(finalDate.year, finalDate.month, finalDate.day);
                                    if (todayAtMidn.getTime() < startDate.getTime()) {
                                        $scope.byday = ((startDate.getTime() - todayAtMidn.getTime()) / 60 / 60 / 24 / 1000) - 1;
                                    } else if (todayAtMidn.getTime() <= lastDate.getTime()) {
                                        $scope.byday = 0;
                                    } else {
                                        $scope.byday = 99999;
                                    }

                                    var dayText = [];
                                    for (var key in dayBase) {
                                        var date = dayBase[key];
                                        $scope.year = date.year;
                                        if (key == 0) {
                                            var preMonth = date.month;
                                            if ($scope.lang == "ja") {
                                                dayText[key] = date.month + "." + date.day + "[" + date.dayName + "]";
                                            } else {
                                                dayText[key] = mNames[date.month - 1] + " " + date.day;
                                            }

                                            if (date.area != null) {
                                                if ($scope.lang == "ja") {
                                                    dayText[key] += "-" + date.area;
                                                } else {
                                                    dayText[key] += "(" + date.area + ")";
                                                }
                                            }
                                        } else if (key == dayBase.length - 1) {
                                            if(preMonth != date.month){
                                                if ($scope.lang == "ja") {
                                                    dayText[key] = date.month + "." + date.day + "[" + date.dayName + "]";
                                                } else {
                                                    dayText[key] = mNames[date.month - 1] + " " + date.day;
                                                }
                                            }
                                            else{
                                                if ($scope.lang == "ja") {
                                                    dayText[key] = date.day + "[" + date.dayName + "]";
                                                } else {
                                                    dayText[key] = date.day;
                                                }
                                            }
                                            var preMonth = date.month;

                                            if (date.area != null) {
                                                if ($scope.lang == "ja") {
                                                    dayText[key] += "-" + date.area;
                                                } else {
                                                    dayText[key] += "(" + date.area + ")";
                                                }
                                            }
                                        }
                                        else {
                                            if(preMonth != date.month){
                                                if ($scope.lang == "ja") {
                                                    dayText[key] =  date.month + "." + date.day + "[" + date.dayName + "]";
                                                } else {
                                                    dayText[key] = mNames[date.month - 1] + " " + date.day;
                                                }
                                            }else{
                                                if ($scope.lang == "ja") {
                                                    dayText[key] = date.day + "[" + date.dayName + "]";
                                                } else {
                                                    dayText[key] = date.day;
                                                }
                                            }
                                            var preMonth = date.month;

                                            if (date.area != null) {
                                                if ($scope.lang == "ja") {
                                                    dayText[key] += "-" + date.area;
                                                } else {
                                                    dayText[key] += "(" + date.area + ")";
                                                }
                                            }
                                        }

                                        var paramText = "";
                                        var sel = "";
                                        if($stateParams.days != undefined){
                                            var daysArray = $stateParams.days.split(',');
                                            if(daysArray.indexOf(key) != -1){
                                                sel = "-no-select";
                                            }
                                            if(daysArray.length > 0){
                                                if(daysArray.indexOf(key) == -1){
                                                    paramText = $stateParams.days + "," + key;
                                                }else{
                                                    daysArray.splice(daysArray.indexOf(key),1);
                                                    for(var dayKey in daysArray){
                                                        paramText += daysArray[dayKey];
                                                        if(daysArray.length -1 != dayKey) {
                                                            paramText += ",";
                                                        }
                                                    }
                                                }
                                            }else{
                                                paramText = key;
                                            }

                                        }else{
                                            paramText = key;
                                        }

                                        $scope.dates.push({text:dayText[key], param:paramText, select:sel});
                                    }
                                }

                                lineDisp(sub_category_id);
                                stationDisp(sub_category_id);

                                if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
                                    $scope.$apply();
                                }

                                check = true;
							}
						}
					}
          		});
          		if(check == false){
                    goMain();
          		}
        	});
	  	});
	});

    function goMain(){
        if($scope.lang == "ja") {
            $state.go('main');
        }else{
            $state.go('lang_main');
        }
    }
	function getDevice(){
	    var ua = navigator.userAgent;
	    if(ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0){
	        return 'sp';
	    }else if(ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0){
	        return 'tab';
	    }else{
	        return 'other';
	    }
	};

	function lineDisp(id) {
  	  	var lineAll = db.ref("/line/line_" + id);
	  	lineAll.on("value", function(dataSnapshot) {
			var preColor = "";
			var tempLines = [];
	    	dataSnapshot.forEach(function(line) {
	    		if(preColor != line.val().color && preColor != ""){
	    			mapLines[preColor] = tempLines;
	    			tempLines = [];
	    		}
	    		tempLines.push(line.val());
	    		preColor = line.val().color
	    	});
	    	mapLines[preColor] = tempLines;
	  	});
	}

	function stationDisp(id) {
        var centerM = 0;
        var centerL = 0;

        var filter_day = [];
        if($stateParams.days != undefined) {
            var daysArray = $stateParams.days.split(',');
            for (var dayKey in daysArray) {
                var dayId = daysArray[dayKey];
                if (daysArray[dayKey].match(/^[0-9]+$/)) {
                    if (daysArray[dayKey] < 0 || daysArray[dayKey] >= sub_category_day.length) {
                        goMain();
                    }
                } else {
                    goMain();
                }

                if(daysArray.indexOf(dayId) != -1){
                    filter_day.push(sub_category_day[dayId]);
                }
            }
        }

        var stationAll = db.ref("/station/station_" + id);
        stationAll.on("value", function(dataSnapshot) {
            var flash = null;
            dataSnapshot.forEach(function(station) {
                if(directOpen == false) {
                    if (paramCenterId) {
                        if (station.val().music_id == paramCenterId) {
                            centerM = station.val().m;
                            centerL = station.val().l;
                            flash = station.val().music_id;
                        }
                    } else if (station.val().music_id == centerId) {
                        centerM = station.val().m;
                        centerL = station.val().l;
                        flash = station.val().music_id;
                    }
                }

                var tempStation = station.val();
                artists[station.val().music_id] = [];

                var tempArtist = "";
                if(tempStation.jp){
                    if ($scope.lang == "en") {
                        tempStation["disp_name"] = station.val().en_name;
                    }else{
                        tempStation["disp_name"] = station.val().jp_name;
                    }
                    var en = station.val().en_name.toLowerCase();
                    var jp = station.val().jp_name.toLowerCase();
                    if(en != jp){
                        artists[station.val().music_id][0] = station.val().jp_name;
                        artists[station.val().music_id][1] = station.val().en_name;
                        tempArtist = station.val().jp_name + "(" + station.val().en_name + ")";
                    }else{
                        artists[station.val().music_id][0] = station.val().jp_name;
                        tempArtist = station.val().jp_name;
                    }
                }else{
                    tempStation["disp_name"] = station.val().en_name;
                    var en = station.val().en_name.toLowerCase();
                    var jp = station.val().jp_name.toLowerCase();
                    if(en != jp){
                        artists[station.val().music_id][0] = station.val().en_name;
                        artists[station.val().music_id][1] = station.val().jp_name;
                        tempArtist = station.val().en_name + "(" + station.val().jp_name + ")";
                    }else{
                        artists[station.val().music_id][0] = station.val().en_name;
                        tempArtist = station.val().en_name;
                    }
                }

                if(directOpen){
                    tempArtist = tempArtist.replace(/\//g, "%2F").replace(/\s/g, "-")
                    if(tempArtist == $stateParams.artist){
                        centerM = station.val().m;
                        centerL = station.val().l;
                        flash = station.val().music_id;
                    }
                }

                var pos = decideXY(station.val().full, station.val().m,  station.val().l);

                tempStation["x"] = pos[0];
                tempStation["y"] = pos[1];

                var tempCount = 0;
                for(var key in tempStation.day) {
                    if (filter_day.indexOf(tempStation.day[key]) != -1) {
                        tempCount++;
                    }
                }

                // console.log(tempStation.day.length);
                // console.log(tempCount);
                // /map/*/*/*/1log(filter_day);
                if(tempStation.day != undefined) {
                    if (tempCount >= tempStation.day.length) {
                        tempStation.no_disp = true;
                    }
                }


                if(tempStation.no_disp){
                    disp_flg[tempStation.music_id] = tempStation.no_disp;
                }

                mapStations[station.val().music_id] = tempStation;

            });

            resetView();
            getSubs();

            var centerX = centerM * reduction + diffW;
            var centerY = centerL * reduction + diffH;

            stopload(centerX/2, centerY/2, flash);

            if(directOpen == false) {
                $translate('map.title').then(function (translations) {
                    if ($scope.lang == "ja") {
                        var sub_category_text = $scope.name + "(" + $scope.name_sub + ")";
                        var title = sub_category_text + translations;
                        ga('send', 'pageview', {
                            'page': "/map/" + $rootScope.replaceUrl(category_name) + "/" + $rootScope.replaceUrl($scope.name + "(" + $scope.name_sub + ")"),
                            'title': title
                        });
                    } else {
                        var sub_category_text = $scope.name;
                        var title = sub_category_text + translations;
                        ga('send', 'pageview', {
                            'page': $scope.lang + "/map/" + $rootScope.replaceUrl(category_name) + "/" + $rootScope.replaceUrl($scope.name + "(" + $scope.name_sub + ")"),
                            'title': title
                        });
                    }
                });
            }
        });
    }

    function show(music_id) {
        openFlg = true;

		if(window.innerWidth < 1000){
			var tpl = 'app/views/detail/detail_mob.html';
			var windowClass = 'my-dialog-mobile';
		}else{
			var tpl = 'app/views/detail/detail.html';
			var windowClass = 'my-dialog';
		}

		modalInstance = $uibModal.open({
			templateUrl: tpl,
			controller: 'DetailController',
			resolve: {
				params: function () {
					return {"sub_category_id":sub_category_id,"music_id":music_id, "disp_flg":disp_flg};
				}
			},
			windowClass: windowClass
		});
        removeAnchor();
		modalInstance.result.then(function (detail_id) {
            directOpen = false;
            addAnchor();
            openFlg = false;
			if($scope.lang == "ja") {
				$state.go('map', {artist: '', index: '', centerId: detail_id}, {notify: false});
			}else{
				$state.go('lang_map', {artist: '', index: '', centerId: detail_id}, {notify: false});
			}
			insertMeta(true);
		}, function (detail_id) {
            directOpen = false;
            addAnchor();
            openFlg = false;
			if(detail_id == "backdrop click"){
				detail_id = $location.search().centerId;

				if (detail_id in mapStations) {
					if($scope.lang == "ja") {
						$state.go('map', {artist: '', index: '', centerId: detail_id}, {notify: false});
					}else{
						$state.go('lang_map', {artist: '', index: '', centerId: detail_id}, {notify: false});
					}
				}
				else{
					if($scope.lang == "ja") {
						$state.go('map', {artist: '', index: '', centerId: ''}, {notify: false});
					}else{
						$state.go('lang_map', {artist: '', index: '', centerId: ''}, {notify: false});
					}
				}
				insertMeta(true);
			}else {
				if (detail_id in mapStations) {
					if ($scope.lang == "ja") {
						$state.go('map', {artist: '', index: '', centerId: detail_id}, {notify: false});
					} else {
						$state.go('lang_map', {artist: '', index: '', centerId: detail_id}, {notify: false});
					}
					insertMeta(false);
					var station = mapStations[detail_id];
					var x = station.m * reduction + diffW;
					var y = station.l * reduction + diffH;

                    var t = $timeout(function () {
                        if(openFlg == false) {
                            setCenter(x / 2, y / 2, true);
                        }
                    }, 1000);


                    var t = $timeout(function () {
                        if(openFlg == false) {
                            flashText(detail_id);
                        }
                    }, 1400);

					var t = $timeout(function () {
                        if(openFlg == false) {
                            show(detail_id);
                        }
					}, 3000);
				} else {
					if ($scope.lang == "ja") {
						$state.go('map', {artist: '', index: '', centerId: ''}, {notify: false});
					} else {
						$state.go('lang_map', {artist: '', index: '', centerId: ''}, {notify: false});
					}

					insertMeta(true);
				}
			}
		});
        $("#nav > ul > li > header > div.main-nav > h1").remove();
        $("#container > div.bread-crumb > ul > li").remove();
    }

	function getSubs(){
		subCategoryAll.orderByChild("category_id").equalTo(category_id).on("value", function(dataSnapshot) {
			dataSnapshot.forEach(function(sub) {
				sub_category_tables[sub.val().sub_category_id] = sub.val();
			});
		});
	}

	function resetView(){
        var innerW = $("#inner").width();
        var innerH = $("#inner").height();
        if(Object.keys(mapStations).length < 100){
            reduction = 0.2;
            radius = 16;
            circleLineWidth = 4;
            lineWidth = 7;
            arc = 12;
            defaultScale = 1.2;
            minScale = 0.99;
            maxScale = 1.4;
            sumScale = 0.2;
            scale = defaultScale;
        }
        zoomMap(innerW/2, innerH/2);
	}

	function zoomMap(clickX, clickY, type){
        var innerW = $("#inner").width();
        var innerH = $("#inner").height();

        if(type == "in"){
	        var oldScale = scale;
		    scale = scale + sumScale;
		    if(scale > maxScale){
		      scale = minScale;
		    }
	    }
	    else if(type == "out"){
	        var oldScale = scale;
	  	    scale = scale - sumScale;
	  	    if(scale < minScale){
	  	      scale = maxScale;
	  	    }
	    }else{
	    	var oldScale = scale;
	    	scale = scale;
	    }

		var topPosition = document.getElementById("inner").scrollTop;
		// 現在の横スクロール位置
		var leftPosition = document.getElementById("inner").scrollLeft;
		var drawScale = function(scaleFrom, scaleTo, innerW, innerH, clickX, clickY, topPosition, leftPosition, deirectOpen){

            alpha = 1.0;
            var ratio = scaleTo/scaleFrom;

			diffW = ((windowWidth + 100) / scaleTo);
			diffH = ((windowHeight + 100) / scaleTo);

		    width = ((100 * m) * reduction) + (diffW * 2);
			height = ((100 * l) * reduction) + (diffH * 2);

		    nowWidth = width*scaleTo;
		    nowHeight = height*scaleTo;

		    $('#map').attr('width', 0);
		    $('#map').attr('height', 0);

		    canvas = $element.find('canvas');
		    canvas.css({'width':nowWidth/2 + 'px', 'height':nowHeight/2 + 'px'});

            $('#map').attr('width', nowWidth);
            $('#map').attr('height', nowHeight);

            $("#map-al").attr('width', nowWidth);
            $("#map-al").attr('height', nowHeight);

            $("#map-al").css({'width': nowWidth/2 + 'px', 'height': nowHeight/2 + 'px'});

            ctx = canvas[0].getContext('2d');
		    ctx.scale(scaleTo, scaleTo);
		    ctx.font = myFont;

		    drawLine(ctx);
		    drawStation(ctx);
		    if(deirectOpen == false){
                addAnchor();
            }

		    var mapClickX =  ((clickX + leftPosition))/scaleFrom;
		    var mapClickY =  ((clickY + topPosition))/scaleFrom;

		    var oldCenterX =  (((innerW/2) + leftPosition))/scaleFrom;
		    var oldCenterY =  (((innerH/2) + topPosition))/scaleFrom;

		    var diffX = (oldCenterX - mapClickX)/ratio;
		    var diffY = (oldCenterY - mapClickY)/ratio;

		}

		if(type == "out" || type == "in"){
		    $('#map').fadeTo(0, 0, function() {
		        drawScale(oldScale, scale, innerW, innerH, clickX, clickY, topPosition, leftPosition,directOpen);
		        $('#map').fadeTo(200, 1);
		    });
		}else{
			drawScale(oldScale, scale, innerW, innerH, clickX, clickY, topPosition, leftPosition,directOpen);
		}

		if(scale + sumScale > maxScale){
		     $("#add-wrapper").fadeTo("fast", 0.3);
		     zoomInDisable = true;
		     $("#add-wrapper").css("pointer-events","none");
		}else{
			 $("#add-wrapper").fadeTo("fast", 0.9);
		     zoomInDisable = false;
		     $("#add-wrapper").css("pointer-events","");
		}

		if( scale - sumScale < minScale){
			  $("#sub-wrapper").fadeTo("fast", 0.3);
			  zoomOutDisable = true;
			  $("#sub-wrapper").css("pointer-events","none");
		}else{
			  $("#sub-wrapper").fadeTo("fast", 0.9);
			  zoomOutDisable = false;
			  $("#sub-wrapper").css("pointer-events","");
		}
    }

	function drawStation(ctx) {
		for(var i in mapStations){
        	var station = mapStations[i];
	        var x = station.m*reduction + diffW;
	        var y = station.l*reduction + diffH;

            ctx.strokeStyle = "#656565";
            ctx.fillStyle = "#FFFFFF";
            ctx.lineWidth = circleLineWidth;
            ctx.beginPath();

    	    ctx.arc(x, y, radius, 0,Math.PI*2,true);
    	    ctx.fill();
    	    ctx.stroke();
        }
    }

	function restoreText(detail_id, alpha) {
		$("id-"+detail_id).fadeTo(0, alpha);
	}

    function removeAnchor() {
        $("#map-al a").remove();
        $("#map-al div").remove();
    }

    function anchorProc(station) {
        var id = station.music_id;
        var text = station.disp_name;
        var spanText = station.disp_name;
        if ($scope.lang == "ja" && station.jp == false) {
            if(station.disp_name.toLowerCase() != station.jp_name.toLowerCase()){
                spanText += "<br>(" + station.jp_name + ")";
            }
        }
        if(station.no_disp == true){
            var tag = $("<div></div>", {
                id:"id-"+id,
                text: text,
                width: textWidth,
                addClass: "artist-no-disp",
                on: {
                    click: function(event) {
                    }
                }
            });
        }else{
            var artist_name = artists[id][0];
            if (1 in artists[id]){
                artist_name += "(" + artists[id][1] + ")";
            }

            if($stateParams.lang == "en"){
                var url = "/en/map/" + category_name + "/" + ($scope.name + "(" + $scope.name_sub + ")").replace(/\//g, "%2F").replace(/\s/g, "-") + "/" + artist_name.replace(/\//g, "%2F").replace(/\s/g, "-") + "/" + station.youtube_id;
            }else{
                var url = "/map/" + category_name + "/" + ($scope.name + "(" + $scope.name_sub + ")").replace(/\//g, "%2F").replace(/\s/g, "-") + "/" + artist_name.replace(/\//g, "%2F").replace(/\s/g, "-") + "/" + station.youtube_id;
            }

            var tag = $("<a></a>", {
                id:"id-"+id,
                text: text,
                width: textWidth,
                href: url,
                addClass: "artist",
                on: {
                    click: function(event) {
                        var idArr = $(this).attr("id").split("-");
                        var id = idArr[1];
                        // イベント設定
                        var station = mapStations[id];
                        var x = station.m * reduction + diffW;
                        var y = station.l  * reduction + diffH;

                        setCenter(x/2, y/2, true);
                        var t = $timeout(function() {
                            if(openFlg == false) {
                                show(id);
                            }
                        }, 800);
                        event.preventDefault();
                        return;
                    }
                }
            });
        }

        if(station.day) {
            var stationText = "";
            stationText += covertTime(station.day, station.time);

            //     for (var n = 0; n < station.day.length; n++) {
            //         stationText += covertTime(station.day[n]);
            //         if(station.time != undefined && station.time[n]){
            //             stationText += " " + station.time[n];
            //         }
            //         if(n != station.day.length -1){
            //             stationText += ", ";
            //         }
            //
            //     }
            spanText += '<br><span class="pop-time">' + setTime + "</span>" + " " + stationText;
        }
        // if(station.day){
        //     var stationText = "";
        //     for (var n = 0; n < station.day.length; n++) {
        //         stationText += covertTime(station.day[n]);
        //         if(station.time != undefined && station.time[n]){
        //             stationText += " " + station.time[n];
        //         }
        //         if(n != station.day.length -1){
        //             stationText += ", ";
        //         }
        //
        //     }
        //     spanText += '<br><span class="pop-time">' + setTime + "</span>" + " " + stationText;
        // }
        if(station.stage) {
            spanText += '<br><span class="pop-stage">' + stage + "</span>" + " " + station.stage;
        }

        var genre = station.genre;
        if(genre != undefined) {
            spanText += "<br>"
            for (var n = 0; n < genre.length; n++) {
                spanText += '<span class="pop-genre">' + genre[n] + "</span> ";
            }
        }

        var span = $("<span></span>", {
            html: spanText,
            addClass: "artist-sp",
        });

        tag.append(span);
        tag.offset({ top: ((station.l*reduction)*den)+(windowHeight/2)+50+station.y, left: ((station.m*reduction)*den)+(windowWidth/2)+50+station.x });
        tag.css({"font-size": fontSize + "px", 'font-weight': 'bold', 'position':'absolute'});
        $("#map-al").append(tag);
    }
	function addAnchor() {
        $("#map-al a").remove();
		$("#map-al div").remove();

        den = scale/2;
        for(var id in mapStations){
		    var station = mapStations[id];
            anchorProc(station);
		}
	}

	function decideXY(full, m, l){
        var r90 = true;
        var r135 = true;
        var r180= true;
        var m_r135 = true;
        var m_r90 = true;
        var m_r45 = true;
        var r0 = true;
        var r45 = true;

        for (var key in full){
        	var direct = full[key];
        	if(direct == "90"){
        		r90 = false;
        	}else if(direct == "135"){
        		r135 = false;
        	}else if(direct == "180"){
        		r180 = false;
        	}else if(direct == "m135"){
        		m_r135 = false;
        	}else if(direct == "m90"){
        		m_r90 = false;
        	}else if(direct == "m45"){
        		m_r45 = false;
        	}else if(direct == "0"){
        		r0 = false;
        	}else if(direct == "45"){
        		r45 = false;
        	}
        }

        //右側全部空いている場合
        if(r90 == true && r135 == true && r45 == true){
            x = 9;
            y = -5;
        }
        //下側全部空いている場合
        else if(m_r45 == true && r0 == true && r45 == true){
            var x = -5;
            var y = 9;
        }
        //下側、右下空いている場合
        else if(r0 == true && r45 == true){
            var x = -5;
            var y = 9;
        }
        //右側,右下空いている場合
        else if(r90 == true && r45 == true){
            var x = 6;
            var y = 6;
        }
        //右側,右上空いている場合
        else if(r90 == true && r135 == true){
            var x = 9;
            var y = -9;
        }
        //右下空いている場合(右、下空いていない)
        else if(r45 == true){
            var x = 6;
            var y = 6;
        }
      　//右空いている場合(右下、右上あいていない)
        else if(r90 == true){
            var x = 9;
            var y = -9;
        }
        else{
            var y = 9;
            var x = -5;
        }
        return [x,y];
	}

	function setCenter(locCenterX, locCenterY, animation) {
		var innerW = $("#inner").width();
		var innerH = $("#inner").height();

		var newScrollX = (locCenterX * scale) - (innerW/2);
		var newScrollY = (locCenterY * scale) - (innerH/2);

		if(animation){
		    $('#inner').scroll();
		    $("#inner").animate({
		      scrollTop: newScrollY
		    }, {
			    duration: 300,
			    queue: false
			}, false);
		    $("#inner").animate({
		    	scrollLeft: newScrollX
			}, {
			    duration: 300,
			    queue: false
			});
		}else{
    	  document.getElementById("inner").scrollLeft = newScrollX;
  	      document.getElementById("inner").scrollTop = newScrollY;
		}

	}

    function drawLine(ctx) {
      	for (var color in mapLines){
        	var lines = mapLines[color];
    		ctx.beginPath();
      		ctx.strokeStyle = color;
      		ctx.lineWidth = lineWidth;

    		for(var i=0; i < lines.length-1; i++){
          		var startX = lines[i].im*reduction + diffW;
          		var startY = lines[i].il*reduction + diffH;

				if(i == 0){
					ctx.moveTo(startX,startY);
				}else{
					if(lines[i+1].direct != lines[i].direct){
						var startX = lines[i+1].im*reduction + diffW;
						var startY = lines[i+1].il*reduction + diffH;
						var endX = lines[i+1].jm*reduction + diffW;
						var endY = lines[i+1].jl*reduction + diffH;
						ctx.arcTo(startX, startY, endX, endY, arc);
					}else if(lines[i-1].direct == lines[i].direct){
						ctx.lineTo(startX, startY);
					}
				}
        	}
	    	var endX = lines[lines.length-1].jm*reduction + diffW;
    	    var endY = lines[lines.length-1].jl*reduction + diffH;
        	ctx.lineTo(endX, endY);
        	ctx.stroke();
      	}
    }

    $('#about-map').click(function () {
        showGuide();
    });

	function showGuide(){
        if(window.innerWidth < 1000){
            var tpl = 'app/views/transferGuide/transferGuide_mob.html';
            var windowClass = 'my-dialog-mobile';
        }else{
            var tpl = 'app/views/transferGuide/transferGuide.html';
            var windowClass = 'my-dialog';
        }

        modalInstance = $uibModal.open({
            templateUrl: tpl,
            controller: 'TransferGuideController',
            resolve: {
                params: function () {
                    return {"sub_category_id":sub_category_id}
                }
            },
            windowClass: windowClass
        });
        removeAnchor();
        $("#nav > ul > li > header > div.main-nav > h1").remove();
        $("#container > div.bread-crumb > ul > li").remove();

        modalInstance.result.then(function () {
            directOpen = false;
            addAnchor();
            openFlg = false;
            if($scope.lang == "ja") {
                $state.go('map', {category: $stateParams.category, sub_category: $stateParams.sub_category, artist: '', index: '', centerId: ''}, {notify: false});
            }else{
                $state.go('lang_map', {category: $stateParams.category, sub_category: $stateParams.sub_category, artist: '', index: '', centerId: ''}, {notify: false});
            }
            insertMeta(true);
        }, function (detail_id) {
            directOpen = false;
            addAnchor();
            openFlg = false;
//            if(detail_id == "backdrop click"){
                if($scope.lang == "ja") {
                    $state.go('map', {category: $stateParams.category, sub_category: $stateParams.sub_category, artist: '', index: '', centerId: ''}, {notify: false});
                }else{
                    $state.go('lang_map', {category: $stateParams.category, sub_category: $stateParams.sub_category, artist: '', index: '', centerId: ''}, {notify: false});
                }
                insertMeta(true);
//            }
        });
    }


    $('#inner').dblclick(function () {
	    var clickX = event.pageX; // X 座標の位置
  	    var clickY = event.pageY; // Y 座標の位置
        zoomMap(clickX, clickY, "in");
    });


    $('#inner').click(function () {
        var clickX = event.clientX; // X 座標の位置
    	var clickY = event.clientY; // Y 座標の位置

    	var topPosition = document.getElementById("inner").scrollTop;
		var leftPosition = document.getElementById("inner").scrollLeft;

	    var mapClickX =  ((clickX + leftPosition))/scale * 2;
	    var mapClickY =  ((clickY + topPosition))/scale * 2;

	    var checkRad = false;
	    //for (var i = 0, len = mapStations.length; i < len; i++) {
	    for(var i in mapStations){
        	var station = mapStations[i];
	        var x = station.m * reduction + diffW;
	        var y = station.l  * reduction + diffH;

	        var a = mapClickX - x;
	        var b = mapClickY - y;
	        var d = Math.sqrt(Math.pow(a,2) + Math.pow(b,2));

	        if(d < radius){
	        	if(station.no_disp != true){
	        		if(station.jp){
	    	    		var name = station.jp_name;
	    	    	}else{
	    	    		var name = station.en_name;
	    	    	}
//	        		changeText(station.music_id, 0.3);
	        		var t1 = $timeout(function() {
	        			setCenter(x/2, y/2, true);
	        		}, 100);
	        		var t2 = $timeout(function() {
	        			if(openFlg == false) {
                            show(station.music_id);
                        }
                    }, 700);
	        		var t3 = $timeout(function() {
	        		   restoreText(station.music_id);
		            }, 1300);
	        	    checkRad = true;
	            }
	        	break;
	        }
	    }

	    if(checkRad != true){
            for (var key in textAreas){
        	    var sx = textAreas[key].sx;
        	    var ex = textAreas[key].ex;
        	    var sy = textAreas[key].sy;
        	    var ey = textAreas[key].ey;

        	    if(mapClickX > sx && mapClickX < ex && mapClickY > sy && mapClickY < ey){
                	var station = mapStations[key];
        	        var x = station.m * reduction+ diffW;
        	        var y = station.l * reduction+ diffH;

        	        changeText(key, 0.3);
        	    	setCenter(x/2, y/2, true);
        	    	var t = $timeout(function() {
                        if(openFlg == false) {
                            show(key);
                        }
					  	restoreText(key);
  	        		}, 600);
        	        break;
                }
            }
	    }
    });

        $scope.addEvent = function() {
            if(device == "other"){
                $('#inner').on('mousedown',function(){press=true;});
                $('#inner').on('mouseup mouseleave',function(){press=false;});

                $('#inner').mousedown(function (event) {
                    $('#inner')
                        .data('down', true)
                        .data('x', event.clientX)
                        .data('y', event.clientY)
                        .data('scrollLeft', this.scrollLeft)
                        .data('scrollTop', this.scrollTop);
                }).css({
                    'overflow': 'hidden',
                });

                $('#inner').mousemove(function (event) {
                    if (press == true) {
                        $('#inner').scrollLeft($('#inner').data('scrollLeft') + $('#inner').data('x') - event.clientX);
                        $('#inner').scrollTop($('#inner').data('scrollTop') + $('#inner').data('y') - event.clientY);
                    }else{
                        var pointX = event.clientX;
                        var pointY = event.clientY;

                        var topPosition = document.getElementById("inner").scrollTop;
                        var leftPosition = document.getElementById("inner").scrollLeft;
                        var mapClickX =  ((pointX + leftPosition))/scale * 2;
                        var mapClickY =  ((pointY + topPosition))/scale * 2;
                        var locCheck = false;
                        var staCheck = false;

                        for(var i in mapStations){
                            var station = mapStations[i];
                            var x = station.m * reduction + diffW;
                            var y = station.l * reduction + diffH;

                            var a = mapClickX - x;
                            var b = mapClickY - y;
                            var d = Math.sqrt(Math.pow(a,2) + Math.pow(b,2));
                            if(d < radius){
                                if(station.no_disp != true){
                                    //外からover
                                    if(checkOver == false){
                                        //        	    changeText(i, 0.3);
                                        if (checkOverNo != null){
                                            if(checkOverNo != i){
                                                restoreText(checkOverNo);
                                                //        	        resetView();
                                            }
                                        }
                                        checkOverNo = i;
                                        $("#map").css("cursor","pointer");
                                    }
                                    locCheck = true;
                                    checkOver = true;
                                    staCheck = true;
                                }
                                break;
                            }
                        }

                        if(staCheck != true){
                            for (var key in textAreas){
                                var sx = textAreas[key].sx;
                                var ex = textAreas[key].ex;
                                var sy = textAreas[key].sy;
                                var ey = textAreas[key].ey;

                                if(mapClickX > sx && mapClickX < ex && mapClickY > sy && mapClickY < ey){
                                    //外からover
                                    if(checkOver == false){
                                        //        	        changeText(key, 0.3);
                                        if (checkOverNo != null){
                                            if(checkOverNo != key){
                                                //    	    	    restoreText(checkOverNo);
                                                //    	    	    resetView();
                                            }
                                        }
                                        checkOverNo = key;
                                        $("#map").css("cursor","pointer");
                                    }
                                    locCheck = true;
                                    checkOver = true;
                                    break;
                                }
                            }
                        }

                        if(locCheck == false){
                            if(checkOver == true){
                                $("#map").css("cursor","default");
                                checkOver = false;
                                checkOverNo = null;
                            }
                        }
                    }
                }).mouseup(function (event) {
                    diffW = ((windowWidth + 100) / scale);
                    diffH = ((windowHeight + 100) / scale);

                    width = ((100 * m) * reduction) + (diffW * 2);
                    height = ((100 * l) * reduction) + (diffH * 2);

                    nowWidth = width*scale;
                    nowHeight = height*scale;
                    $("#inner").data('down', false);
                });
            }
            document.getElementById("map").addEventListener('touchstart', function(e) {
                // ビューポートの変更を防止

                var clickX = event.changedTouches[0].pageX; // X 座標の位置
                var clickY = event.changedTouches[0].pageY; // Y 座標の位置

                //シングルタップ判定
                if( !didFirstTap ) {
                    // 1回目のタップ判定を真にする
                    didFirstTap = true ;
                    didFirstX = clickX;
                    didFirstY = clickY;

                    // 350ミリ秒だけ、1回目のタップ判定を残す
                    setTimeout( function() {
                        didFirstTap = false ;
                        didFirstX = null;
                        didFirstY = null;
                    }, 350 ) ;

                    // ダブルタップの判定
                } else if(didFirstX-clickX < 30 &&  didFirstY-clickY < 30) {
                    // ダブルタップイベントの実行
                    zoomMap(didFirstX, didFirstY, "in");
                    // 1回目のタップ判定を解除
                    didFirstTap = false ;
                }
            });
        }
        $("#inner").scroll(function () {
		didFirstTap = false ;
	});

	this.zoomInMap = function(){
	    if(zoomInDisable != true){
	        var innerW = $("#inner").width();
	        var innerH = $("#inner").height();
	        zoomMap(innerW/2, innerH/2, "in");
	    }
	}

	this.zoomOutMap = function(){
	    if(zoomOutDisable != true){
	        var innerW = $("#inner").width();
	        var innerH = $("#inner").height();
	        zoomMap(innerW/2, innerH/2, "out");
	    }
	}

	$scope.goSearchRsult = function($item, $model, $label) {
		if($item.transition){
            if($scope.lang == "ja") {
                $state.go('map', {
                    category: category_name,
                    sub_category: $item.url_param.replace(/\//g, "%2F").replace(/\s/g, "-"),
                    artist: '',
                    index: '',
                    centerId: $item.id
                }, {notify: true});
            }else{
                $state.go('lang_map', {
                    lang: "en",
                    category: category_name,
                    sub_category: $item.url_param.replace(/\//g, "%2F").replace(/\s/g, "-"),
                    artist: '',
                    index: '',
                    centerId: $item.id
                }, {notify: true});
			}
		}else{
	        var station = mapStations[$item.id];
	        var x = station.m * reduction+ diffW;
	        var y = station.l * reduction+ diffH;

            if($scope.lang == "ja") {
                $state.go('map', {artist: '', index: '', centerId: $item.id}, {notify: false});
            }else{

                $state.go('lang_map', {artist: '', index: '', centerId: $item.id}, {notify: false});
			}
		    setCenter(x/2, y/2, true);
		    flashText($item.id);

		}
	};

	function flashText(detail_id){
		if(detail_id in disp_flg){
			return;
		}

		var timer_id;
		var i = 0;

		timer_id = setInterval(function(){
			var opacity = $("#id-"+detail_id).css('opacity');
			if(opacity == 0 ){
				$("#id-"+detail_id).css('opacity', 1);
			}else{
				$("#id-"+detail_id).css('opacity', 0);
			}
			if(i == 7){
			    clearInterval(timer_id);
			    $("#id-"+detail_id).css('opacity', 1);
			}
	        i++;
	    },200);
	}

	function insertMeta(count) {
        $translate('common.keywords').then(function (translations) {
            $("meta[name=keywords]").attr('content', translations);
        });

        if($scope.lang == "ja") {
            var sub_category_text = $scope.name + "(" + $scope.name_sub + ")";
            var url = 'https://meetrow.com/map/' + $rootScope.myEncode(category_name) + "/" + $rootScope.myEncode($scope.name + "(" + $scope.name_sub + ")");
        }else{
            var sub_category_text = $scope.name;
            var url = 'https://meetrow.com/' + $scope.lang +'/map/' + $rootScope.myEncode(category_name) + "/" + $rootScope.myEncode($scope.name + "(" + $scope.name_sub + ")");
        }

        $('link[rel="canonical"]').attr('href', url);
        $('meta[property="og:url"]').attr('content', url);

        $("html").find('link[rel="alternate"]').each(function() {
            if($(this).attr("hreflang") == "ja"){
                var url = 'https://meetrow.com/map/' + $rootScope.myEncode(category_name) + "/" + $rootScope.myEncode($scope.name + "(" + $scope.name_sub + ")");
                $(this).attr("href", url);
            }
            else{
                var url = 'https://meetrow.com/' + $(this).attr("hreflang") +'/map/' + $rootScope.myEncode(category_name) + "/" + $rootScope.myEncode($scope.name + "(" + $scope.name_sub + ")");
                $(this).attr("href", url);
            }
        });

        $translate('map.title').then(function (translations) {
            document.title = sub_category_text + translations;
            $("title").text(sub_category_text + translations);
            $('meta[property="og:title"]').attr('content', sub_category_text + translations);
            if (count){
            	if($scope.lang == "ja") {
                    ga('send', 'pageview', {
                        'page': "map/" + $rootScope.replaceUrl(category_name) + "/" + $rootScope.replaceUrl($scope.name + "(" + $scope.name_sub + ")"),
                        'title': sub_category_text + translations
                    });
                }else{
                    ga('send', 'pageview', {
                        'page': $scope.lang + "/map/" + $rootScope.replaceUrl(category_name) + "/" + $rootScope.replaceUrl($scope.name + "(" + $scope.name_sub + ")"),
                        'title': sub_category_text + translations
                    });
                }
            }

        });

        $translate('map.description').then(function (translations) {
            $("meta[name=description]").attr('content', translations.replace( /\{sub_category_text\}/g , sub_category_text ));
            $('meta[property="og:description"]').attr('content', translations.replace( /\{sub_category_text\}/g , sub_category_text));
        });

        $translate('map.h1Text').then(function (translations) {
            $("#nav > ul > li > header > div.main-nav > h1").remove();
            $("#container > div.bread-crumb > ul > li").remove();

            $scope.h1Text = translations.replace( /\{sub_category_text\}/g , sub_category_text ) ;
            $("#container > div.bread-crumb > ul").append('<li class="top"><a href="/">Top</a></li>');
            $("#nav > ul > li > header > div.main-nav").prepend('<h1>' + $scope.h1Text + '</h1>');
            $("#container > div.bread-crumb > ul").append('<li class="def">&nbsp;＞&nbsp;' + $scope.h1Text + '</li>');
        });

        $translate('common.siteName').then(function (translations) {
            $('meta[property="og:site_name"]').attr('content', translations);
        });

    }

    function covertTime(days, times){
        var dayBase = [];
        for (var key in days) {
            var dayAndArea = days[key].split("-");
            var area = null;

            if(dayAndArea.length > 1){
                var dayArr = dayAndArea[1].split("/");
                if(0 in dayAndArea){
                    area = dayAndArea[0];
                }
            }else{
                var dayArr = dayAndArea[0].split("/");
            }

            var time = null;
            if(times != undefined) {
                if (key in times) {
                    time = times[key];
                }
            }

            var dateObj = new Date(dayArr[0], dayArr[1] - 1, dayArr[2]);
            var day = dateObj.getDay();
            var dayName = dayNames[day];

            dayBase.push({
                year: dayArr[0],
                month: dayArr[1],
                day: dayArr[2],
                dayName: dayName,
                area: area,
                time: time
            });
        }

        var dayText ="";
        if (dayBase.length > 1) {
            for (var key in dayBase) {
                var date = dayBase[key];
                if (key == 0) {
                    if ($scope.lang == "ja") {
                        dayText = date.year + "." + date.month + "." + date.day + "[" + date.dayName + "]";
                        if (date.area != null) {
                            dayText += "-" + date.area;
                        }
                        if (date.time != null) {
                            dayText += " " + date.time;
                        }
                    } else {
                        dayText = mNames[date.month - 1] + " " + date.day;

                        if (date.time != null) {
                            dayText += " at " + date.time;
                        }
                        if (date.area != null) {
                            dayText += " (" + date.area + ")";
                        }
                    }


                } else if (key == dayBase.length - 1) {
                    if ($scope.lang == "ja") {
                        dayText += "/" + date.month + "." + date.day + "[" + date.dayName + "]";
                        if (date.area != null) {
                            dayText += "-" + date.area;
                        }
                        if (date.time != null) {
                            dayText += " " + date.time;
                        }
                    }else{
                        dayText += ", " + date.day;
                        if (date.time != null) {
                            dayText += " at " + date.time;
                        }
                        if (date.area != null) {
                            dayText += " (" + date.area + ")";
                        }

                        dayText += ",  " + date.year;
                    }
                }
                else {
                    if ($scope.lang == "ja") {
                        dayText += "/" + date.month + "." + date.day + "[" + date.dayName + "]";
                        if (date.area != null) {
                            dayText += "-" + date.area;
                        }
                        if (date.time != null) {
                            dayText += " " + date.time;
                        }
                    }else{
                        dayText += ", " + date.day;
                        if (date.time != null) {
                            dayText += " at " + date.time;
                        }
                        if (date.area != null) {
                            dayText += " (" + date.area + ")";
                        }
                    }
                }
            }
        }else if (dayBase.length == 1) {
            var date = dayBase[0];
            if ($scope.lang == "ja") {
                dayText += date.year + "." + date.month + "." + date.day + "[" + date.dayName + "]";
                if (date.area != null) {
                    dayText += "-" + date.area;
                }
                if (date.time != null) {
                    dayText += " " + date.time;
                }

            } else {
                dayText = mNames[date.month - 1] + " " + date.day;
                if (date.time != null) {
                    dayText += " at " + date.time;
                }
                if (date.area != null) {
                    dayText += " (" + date.area + ")";
                }
                dayText += ",  " + date.year;
            }
        }
        return dayText;
    }
  }
})();