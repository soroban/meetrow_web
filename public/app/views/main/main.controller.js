
;(function() {
    'use strict';

    angular
        .module('public-app')
        .controller('MainController', MainController);

    MainController.$inject = ['$state', '$scope', '$stateParams','$location', '$timeout', '$rootScope', '$translate', '$sce'];

    function MainController($state, $scope, $stateParams, $location, $timeout, $rootScope, $translate, $sce){
        var lang;
        if($stateParams.lang == "en"){
            $scope.lang = "en";
            $scope.lang_str = "en";
            $scope.main_link = "/" + $scope.lang_str;
            $scope.sub_link = "/" + $scope.lang_str + "/local";
            $translate.use('en');
            $("html").attr("lang", "en");
        }else{
            $scope.lang = "ja";
            $scope.lang_str = "";
            $scope.main_link = "/";
            $scope.sub_link = "/global";
            $translate.use('ja');
            $("html").attr("lang", "ja");
        }

        $('.years-content').css('display','none');
        $('#fountanWrap').height("200").css('display','block');


        if($stateParams.global == true){
            if($scope.lang == "ja"){
                $translate('sub.h1Text').then(function (translations) {
                    $scope.h1Text = translations;
                    $("#nav > ul > li > header > div.main-nav").prepend('<h1>' + translations + '</h1>');
                });
                $translate('sub.title').then(function (translations) {
                    document.title = translations;
                    $("title").text(translations);
                    $('meta[property="og:title"]').attr('content',translations);

                    ga('send', 'pageview', {
                        'page': '/global',
                        'title': translations
                    });
                });

                $translate('sub.description').then(function (translations) {
                    $("meta[name=description]").attr('content', translations);
                    $('meta[property="og:description"]').attr('content', translations);
                });

                $('link[rel="canonical"]').attr('href','https://meetrow.com/global');
                $('meta[property="og:url"]').attr('content', 'https://meetrow.com/global');
                $scope.main = "japan_nosel";
                $scope.sub = "globe";

                $translate('sub.areaTxt').then(function (translations) {
                    $scope.area_txt = translations;
                });
            }else{
                $translate('main.h1Text').then(function (translations) {
                    $scope.h1Text = translations;
                    $("#nav > ul > li > header > div.main-nav").prepend('<h1>' + translations + '</h1>');
                });
                $translate('main.title').then(function (translations) {
                    document.title = translations;
                    $("title").text(translations);
                    $('meta[property="og:title"]').attr('content',translations);
                    ga('send', 'pageview', {
                        'page': '/en',
                        'title': translations
                    });
                });

                $translate('main.description').then(function (translations) {
                    $("meta[name=description]").attr('content', translations);
                    $('meta[property="og:description"]').attr('content', translations);
                });

                $('link[rel="canonical"]').attr('href','https://meetrow.com/' + $scope.lang_str);
                $('meta[property="og:url"]').attr('content', 'https://meetrow.com/' + $scope.lang_str);
                $('meta[property="og:url"]').attr('content', 'https://meetrow.com/' + $scope.lang_str);
                $scope.main = "globe";
                $scope.sub = "japan_nosel";

                $translate('main.areaTxt').then(function (translations) {
                    $scope.area_txt = translations;
                });
            }
            $translate('common.keywords').then(function (translations) {
                $("meta[name=keywords]").attr('content', translations);
            });
            $('meta[property="og:url"]').attr('content', 'https://meetrow.com'  + $scope.lang_str);

            $("html").find('link[rel="alternate"]').each(function() {
                if($(this).attr("hreflang") == "ja"){
                    $(this).attr("href", "https://meetrow.com/global");
                }
                else{
                    $(this).attr("href", "https://meetrow.com/" + $(this).attr("hreflang"));
                }
            });
        }else{
            if($scope.lang == "ja"){
                $translate('main.h1Text').then(function (translations) {
                    $scope.h1Text = translations;
                    $("#nav > ul > li > header > div.main-nav").prepend('<h1>' + translations + '</h1>');
                });
                $translate('main.title').then(function (translations) {
                    document.title = translations;
                    $("title").text(translations);
                    $('meta[property="og:title"]').attr('content',translations);
                    ga('send', 'pageview', {
                        'page': '/',
                        'title': translations
                    });
                });

                $translate('main.description').then(function (translations) {
                    $("meta[name=description]").attr('content', translations);
                    $('meta[property="og:description"]').attr('content', translations);
                });

                $('link[rel="canonical"]').attr('href','https://meetrow.com');
                $('meta[property="og:url"]').attr('content', 'https://meetrow.com');
                $scope.main = "japan";
                $scope.sub = "globe_nosel";

                $translate('main.areaTxt').then(function (translations) {
                    $scope.area_txt = translations;
                });

                $("html").find('link[rel="alternate"]').each(function() {
                    if($(this).attr("hreflang") == "ja"){
                        $(this).attr("href", "https://meetrow.com/global");
                    }
                    else{
                        $(this).attr("href", "https://meetrow.com/" + $(this).attr("hreflang"));
                    }
                });

            }else{
                $translate('sub.h1Text').then(function (translations) {
                    $scope.h1Text = translations;
                    $("#nav > ul > li > header > div.main-nav").prepend('<h1>' + translations + '</h1>');
                });
                $translate('sub.title').then(function (translations) {
                    document.title = translations;
                    $("title").text(translations);
                    $('meta[property="og:title"]').attr('content',translations);
                    ga('send', 'pageview', {
                        'page': '/en/local',
                        'title': translations
                    });
                });

                $translate('sub.description').then(function (translations) {
                    $("meta[name=description]").attr('content', translations);
                    $('meta[property="og:description"]').attr('content', translations);
                });

                $('link[rel="canonical"]').attr('href','https://meetrow.com/' + $scope.lang_str + '/local');
                $('meta[property="og:url"]').attr('content', 'https://meetrow.com/' + $scope.lang_str + '/local');
                $scope.main = "globe_nosel";
                $scope.sub = "japan";

                $translate('sub.areaTxt').then(function (translations) {
                    $scope.area_txt = translations;
                });
            }
            $translate('common.keywords').then(function (translations) {
                $("meta[name=keywords]").attr('content', translations);
            });

            $("html").find('link[rel="alternate"]').each(function() {
                if($(this).attr("hreflang") == "ja"){
                    $(this).attr("href", "https://meetrow.com");
                }
                else{
                    $(this).attr("href", "https://meetrow.com/" + $(this).attr("hreflang") + "/local");
                }
            });

        }

        $translate('common.siteName').then(function (translations) {
            $('meta[property="og:site_name"]').attr('content', translations);
        });


        function stopload(){
            $('.loading').delay(0).fadeOut(600).queue(function() {
                $('.years-content').fadeIn(500)
            });
        }

        $scope.subs_by_years = [];
        $scope.category_name = "音楽";
        var db = firebase.database();
        var category_id = 1;
        var searchAll = db.ref("/search_summary/1");
        var subCategoryAll = db.ref("/sub_category");

        var sub_categories = [];

        subCategoryAll.orderByChild("order").once("value").then(function(dataSnapshot) {
            dataSnapshot.forEach(function(sub) {
                var state = sub.val().state;
                if (!(sub.val().category_id == category_id && sub.val().state == 2)) {
                } else {
                    if (sub.val().sub_category_id == 9) {
                        var tempVal = sub.val();
                        tempVal["new"] = true;
                    } else {
                        var tempVal = sub.val();
                        tempVal["new"] = false;
                    }
                    sub_categories[sub.val().sub_category_id] = sub.val();

                    var store = false;
                    switch ($stateParams.global) {
                        case true:
                            if (sub.val().country != "jp" && sub.val().country != "n/a") {
                                store = true;
                            }
                            break;
                        default:
                            if (sub.val().country == "jp" || sub.val().country == "n/a") {
                                store = true;
                            }
                            break;
                    }

                    if (store) {
                        var days = sub.val().day;
                        if (days) {
                            var dayAndArea = days[0].split("-");
                            if (dayAndArea.length > 1) {
                                var dayArr = dayAndArea[1].split("/");
                            } else {
                                var dayArr = dayAndArea[0].split("/");
                            }
                            var year = dayArr[0];

                            if (year in $scope.subs_by_years == false) {
                                $scope.subs_by_years[String(year)] = [];
                            }

                            $scope.subs_by_years[String(year)].push(tempVal);
                        }
                        else {
                            if ('other' in $scope.subs_by_years == false) {
                                $scope.subs_by_years['other'] = [];
                            }
                            $scope.subs_by_years['other'].push(tempVal);
                        }
                    }
                }
            });
            stopload();
            if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
                $scope.$apply();
            }
        });

        $scope.hover = function(){
            var img = document.getElementById("fes-image");
            img.style.webkitFilter = "brightness(0)";
        }

        $scope.selectList = function(value) {
            var priJson = [];
            var retJson = [];
            var value = value.replace(/[ぁ-ん]/g, function(s) {
                return String.fromCharCode(s.charCodeAt(0) + 0x60);
            });

            return searchAll.orderByChild("keyword").equalTo(value.toLowerCase()).once("value").then(function (dataSnapshot) {
                dataSnapshot.forEach(function(search) {
                    var subCat = sub_categories[search.val().sub_category_id];
                    if($scope.lang == "ja"){
                        retJson.push({
                            "disp": search.val().disp_str,
                            "url_param": subCat.name + "(" + subCat.name_sub + ")",
                            "fes": subCat.name + "(" + subCat.name_sub + ")",
                            "id": search.val().id,
                            "transition": true
                        });
                    }else {
                        retJson.push({
                            "disp": search.val().disp_str,
                            "url_param": subCat.name + "(" + subCat.name_sub + ")",
                            "fes": subCat.name,
                            "id": search.val().id,
                            "transition": true
                        });
                    }
                });
                return priJson.concat(retJson);
            });
        }

        $scope.goSearchRsult = function($item, $model, $label) {
            if($item.transition){
                if($scope.lang == "ja") {
                    $state.go('map', {
                        category: $scope.category_name,
                        sub_category:$item.url_param.replace(/\//g, "%2F").replace(/\s/g, "-"),
                        artist:'',
                        title:'',
                        centerId:$item.id
                    },{notify: true});
                }else {
                    $state.go('lang_map', {
                        lang: "en",
                        category: $scope.category_name,
                        sub_category: $item.url_param.replace(/\//g, "%2F").replace(/\s/g, "-"),
                        artist: '',
                        title: '',
                        centerId: $item.id
                    }, {notify: true});
                }
            }else{
                var station = mapStations[$item.id];
                var x = station.m + diffW;
                var y = station.l + diffH;

                if($scope.lang == "ja"){
                    $state.go('map', {lang: "", artist:'', title:'', centerId:$item.id},{notify: false});
                }else{
                    $state.go('map', {lang: "en", artist:'', title:'', centerId:$item.id},{notify: false});
                }
                setCenter(x/2, y/2, true);
                flashText($item.id);
            }
        };
    }
})();
