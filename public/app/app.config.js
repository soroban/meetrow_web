;(function() {
    'use strict';

    angular
        .module('public-app')
        .config(AppConfig).run(AppRun)
        .directive('pageTitle', AppDirective);

        AppConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$translateProvider', '$analyticsProvider', '$qProvider'];
        AppRun.$inject = ['$rootScope', '$route', '$location', '$routeParams'];

    function AppConfig($stateProvider, $urlRouterProvider, $locationProvider, $translateProvider, $analyticsProvider, $qProvider) {
        $analyticsProvider.virtualPageviews(false);
        $analyticsProvider.firstPageview(false);
        $qProvider.errorOnUnhandledRejections(false);
        $translateProvider.useSanitizeValueStrategy('escapeParameters');

        $stateProvider
        .state({
            name: 'main',
            url: '/',
            templateUrl: 'app/views/main/main.html',
            controller: 'MainController',
            reloadOnSearch: false,
            controllerAs: 'vm'
        })
        .state({
            name: 'lang_main',
            url: '/en',
            params: {lang:"en", global:true},
            templateUrl: 'app/views/main/main.html',
            controller: 'MainController',
            reloadOnSearch: false,
            controllerAs: 'vm'
        })
        .state({
            name: 'global',
            url: '/global',
            params: {global:true},
            templateUrl: 'app/views/main/main.html',
            controller: 'MainController',
            reloadOnSearch: false,
            controllerAs: 'vm'
        })
        .state({
            name: 'local',
            url: '/en/local',
            params: {lang:"en"},
            templateUrl: 'app/views/main/main.html',
            controller: 'MainController',
            reloadOnSearch: false,
            controllerAs: 'vm'
        }).state({
            name: 'lang_guide',
            url: '/en/guide/:category/:sub_category?centerId',
            params: {guide:true, lang:"en", category: null, sub_category: null},
            templateUrl: 'app/views/map/map.html',
            controller: 'MapController',
            reloadOnSearch: false,
            controllerAs: 'vm'
        }).state({
            name: 'guide',
            url: '/guide/:category/:sub_category?centerId',
            params: {guide:true, category: null, sub_category: null},
            templateUrl: 'app/views/map/map.html',
            controller: 'MapController',
            reloadOnSearch: false,
            controllerAs: 'vm'
        })
        .state({
            name: 'lang_map',
            url: '/en/map/:category/:sub_category/:artist/:index?centerId&days',
            params: {lang:"en", category: null, sub_category: null, artist : {squash: true, value:null}, index : {squash: true, value:null}, "days": {squash: false, value:null}},
            templateUrl: 'app/views/map/map.html',
            controller: 'MapController',
            reloadOnSearch: false,
            controllerAs: 'vm'
        }).state({
            name: 'map',
            url: '/map/:category/:sub_category/:artist/:index?centerId&days',
            params: {category: null, sub_category: null, artist : {squash: true, value:null}, index : {squash: true, value:null}, "days": {squash: false, value:null}},
            templateUrl: 'app/views/map/map.html',
            controller: 'MapController',
            reloadOnSearch: false,
            controllerAs: 'vm'
        }).state('sitemap.xml', {url: '/sitemap.xml'});

        $urlRouterProvider.otherwise('/');
        $locationProvider
	    .html5Mode(
	    {
	        enabled: true,
	        requireBase: false
	    });

        $translateProvider.useStaticFilesLoader({
            prefix : 'lang/lang_',
            suffix : '.json'
        });
        $translateProvider.preferredLanguage('ja');
        $translateProvider.fallbackLanguage('en');
       // $translateProvider.useSanitizeValueStrategy('sanitize');
    }
  
    function AppRun($rootScope, $route, $location, $routeParams) {
	    $rootScope.$on('$locationChangeSuccess', function() {
	        $rootScope.actualLocation = $location.path();
	    });        
	 
	    $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
	        if($rootScope.actualLocation === newLocation) {
	        	window.location.reload();
	            //alert('Why did you use history back?');
	        }
	    });
    }
  
    function AppDirective($rootScope) {
        return {
            link: function(scope, element) {
	            var listener = function(event, state) {
		            var pageTitle = '';
		            if (state.title) {
		                pageTitle = state.title + ' | SiteName';
		                element.text(pageTitle);
		            }
		        };
		        // stateの変更が完了した後に処理を実行
		        $rootScope.$on('$stateChangeSuccess', listener);
	        }
        };
    }
})();
